package com.devimpact.bhelp.bhelpkotlin.services

import android.app.AlertDialog
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.os.Handler


class NetworkBroadcastReceiver : BroadcastReceiver() {

    private var alreadyOnline = true

    override fun onReceive(context: Context?, intent: Intent?) {

        Handler().postDelayed({
            if (NetworkUtil.getConnectivityStatus(context!!) == 0 ) {
                alreadyOnline = false

                val builder = AlertDialog.Builder(context)
                builder.setTitle("Error de conexión")
                builder.setMessage("En este momento no tienes conxeión a internet. Tus datos se guardarán localmente y serán sincronizados cuando recuperes conexión.")
                builder.setPositiveButton("Aceptar") { _, _ ->
                }
                val dialog = builder.create()
                dialog.show()

            } else if (!alreadyOnline){
                alreadyOnline = true
            }
        }, 1000)


    }

}
