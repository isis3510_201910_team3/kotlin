package com.devimpact.bhelp.bhelpkotlin.activity

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.support.v7.app.AppCompatActivity
import com.devimpact.bhelp.bhelpkotlin.R

class SplashScreenActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splashscreen)
        supportActionBar!!.hide()

        Handler().postDelayed({

            val sharedPref = getSharedPreferences(getString(R.string.preference_file_key), Context.MODE_PRIVATE)
            val correo = sharedPref.getString("correoUsuario","")
            var intent: Intent
            intent = if(correo == "") {
                Intent(this, LoginActivity::class.java)
            } else {
                Intent(this, Menu::class.java)
            }
            startActivity(intent)
        }, 1000)
    }

}