package com.devimpact.bhelp.bhelpkotlin.adapters

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentTransaction
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.TextView
import com.devimpact.bhelp.bhelpkotlin.R
import com.devimpact.bhelp.bhelpkotlin.activity.Menu
import com.devimpact.bhelp.bhelpkotlin.dialogs.FullScreenDialogEditHijo
import com.devimpact.bhelp.bhelpkotlin.fragments.logic.FragmentHijos
import com.devimpact.bhelp.bhelpkotlin.fragments.logic.FragmentHomeTabs
import com.devimpact.bhelp.bhelpkotlin.models.Hijo


class HijoListAdapter(fragmentHijos: FragmentHijos, hijos: ArrayList<Hijo>, hijo_item: Int, correoUsuario: String) : RecyclerView.Adapter<HijoListAdapter.SingleItemRowHolder>() {

    private var mContext: FragmentHijos = fragmentHijos
    private var itemsList: ArrayList<Hijo> = hijos
    private var cU = correoUsuario

    var layout: Int = hijo_item
    lateinit var nombre: TextView
    lateinit var edad: TextView

    private lateinit var supportFragmentManager: FragmentManager

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): SingleItemRowHolder {
        val v = LayoutInflater.from(viewGroup.context).inflate(layout, null)
        supportFragmentManager = mContext!!.supportFragmentManager


        return SingleItemRowHolder(v)
    }

    override fun onBindViewHolder(holder: SingleItemRowHolder, i: Int) {
        val item = itemsList[i]

        holder.nombre.tag = i
        holder.nombre.text = item.nombre
        holder.edad.text = item.edad.toString()
        holder.id = item.id
        holder.peso.text = item.peso.toString()
        holder.altura.text = item.altura.toString()
        holder.fecha.text = item.fechaNacimiento
        if (item.compartido == 1) {
            holder.compartido.visibility = View.VISIBLE
        }

        if (item.bebe == 1) {
            holder.avatar.setBackgroundResource(R.drawable.ic_baby)
        } else if (item.sexo == 1 && item.bebe == 0) {
            holder.avatar.setBackgroundResource(R.drawable.ic_girl)
        } else {
            holder.avatar.setBackgroundResource(R.drawable.ic_boy)
        }

    }

    override fun getItemCount(): Int {
        return itemsList.size
    }

    inner class SingleItemRowHolder(view: View) : RecyclerView.ViewHolder(view) {

        var nombre: TextView = view.findViewById(R.id.txt_nombre_hijo)
        var edad: TextView = view.findViewById(R.id.txt_edad_hijo)
        var avatar: ImageView = view.findViewById(R.id.avatar_hijo)
        var edit: ImageButton = view.findViewById(R.id.editHijo)
        var compartido: ImageView = view.findViewById(R.id.compartidoHijo)
        lateinit var id: String
        var peso: TextView = view.findViewById(R.id.txt_peso_hijo)
        var altura: TextView = view.findViewById(R.id.txt_altura_hijo)
        var fecha: TextView = view.findViewById(R.id.txt_fecha_hijo)

        init {
            view.setOnClickListener { v ->
                val position = Integer.parseInt(nombre.tag.toString())
                val item = itemsList[position]

                val shared = v.context.getSharedPreferences(v.context.getString(R.string.preference_file_key), Context.MODE_PRIVATE)
                shared.edit().putBoolean("desdeProfileHome", true)
                        .putString("nombre", nombre.text.toString())
                        .putString("peso", peso.text.toString())
                        .putString("altura", altura.text.toString())
                        .putInt("bebe", item.bebe)
                        .putInt("sexo", item.sexo)
                        .putString("active", "FragmentProfileHome")
                        .putString("idHijo", item.id)
                        .commit()

                supportFragmentManager = mContext!!.supportFragmentManager
                val fragmentHijos = supportFragmentManager.findFragmentByTag("fragmentHijos")
                val fragmentHomeTabs = supportFragmentManager.findFragmentByTag("fragmentHomeTabs") as FragmentHomeTabs
                fragmentHomeTabs.refresh()
                //fragmentProfileHome.refresh()
                supportFragmentManager.beginTransaction().hide(fragmentHijos!!)
                        .show(fragmentHomeTabs!!).commit()

                val intent = Intent(v.context, Menu::class.java)
                intent.putExtra("correo", cU)
                intent.putExtra("desdeFragmentHijos", true)
                //(v.context as Activity).finish()
                //startActivity(v.context, intent, null)


            }

            edit.setOnClickListener {
                crearFullScreenDialog(id, nombre.text.toString(), peso.text.toString(), altura.text.toString(), edad.text.toString(), fecha.text.toString())
            }

        }
    }

    private fun crearFullScreenDialog(id: String, nombre: String, peso: String, altura: String, edad: String, fecha:String) {
        val fragmentManager = mContext.fragmentManager
        val newFragment = FullScreenDialogEditHijo()
        val args = Bundle()
        args.putString("fecha", fecha)
        args.putString("peso", peso)
        args.putString("altura", altura)
        args.putString("nombre", nombre)
        args.putString("edad", edad)
        args.putString("id", id)

        newFragment.arguments = args

        val transaction = fragmentManager!!.beginTransaction()
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
        transaction.add(android.R.id.content, newFragment, "FullScreenDialogEditHijo")
                .addToBackStack(null) //agregado para poder ir atrás
                .commit()
    }
}