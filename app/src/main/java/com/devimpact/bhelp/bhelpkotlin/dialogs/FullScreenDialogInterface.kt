package com.devimpact.bhelp.bhelpkotlin.dialogs

import android.support.v4.app.DialogFragment
import com.devimpact.bhelp.bhelpkotlin.databasehelpers.DatabaseHelper
import com.devimpact.bhelp.bhelpkotlin.models.TareaAdapter

abstract class FullScreenDialogInterface : DialogFragment() {

    fun guardarTareaLocal(titulo: String, fecha: String, hora: String, categoria: String, prioridad: String, descripcion: String, compartido:Int, dataBaseHelper: DatabaseHelper) : Boolean{

        val idG = "$titulo-$fecha $hora"
        val tarea = TareaAdapter(idG,titulo,"$fecha $hora",prioridad,categoria,compartido,descripcion,"")

        return dataBaseHelper.addData(idG, tarea, categoria)
    }
}