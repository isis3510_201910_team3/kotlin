package com.devimpact.bhelp.bhelpkotlin.adapters

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter
import com.devimpact.bhelp.bhelpkotlin.fragments.logic.FragmentEvaluarConCamara
import com.devimpact.bhelp.bhelpkotlin.fragments.logic.FragmentProfileHome
import com.devimpact.bhelp.bhelpkotlin.fragments.logic.FragmentReconocerVoz

class AdapterFragmentTabsHome(fm: FragmentManager, correoUsuario: String) : FragmentStatePagerAdapter(fm) {

    override fun getItem(position: Int): Fragment {
        return when (position) {
            0 -> {
                FragmentProfileHome()
            }
            1 -> FragmentEvaluarConCamara()
            else -> {
                return FragmentReconocerVoz()
            }
        }
    }

    override fun getCount(): Int {
        return 3
    }

    override fun getPageTitle(position: Int): CharSequence {
        return when (position) {
            0 -> "Resumen"
            1 -> "Crecimiento"
            else -> {
                return "Reconocer voz"
            }
        }
    }
}