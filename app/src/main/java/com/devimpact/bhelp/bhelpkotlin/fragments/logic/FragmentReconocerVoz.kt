package com.devimpact.bhelp.bhelpkotlin.fragments.logic

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.app.Dialog
import android.media.MediaPlayer
import android.media.MediaRecorder
import android.os.Build
import android.os.Bundle
import android.support.v4.app.Fragment
import java.io.IOException
import java.util.*
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.annotation.RequiresApi
import com.devimpact.bhelp.bhelpkotlin.R
import com.devimpact.bhelp.bhelpkotlin.adapters.AudioListAdapter
import kotlinx.android.synthetic.main.fragment_reconocer_voz.*
import java.io.File
import kotlin.collections.ArrayList


class FragmentReconocerVoz : Fragment() {

    private var fileName: String = ""

    private var recording: Boolean = false
    private var bool: Boolean = false

    private var recorder: MediaRecorder? = null

    private lateinit var audioView: RecyclerView
    private lateinit var audioListAdapter: AudioListAdapter
    private lateinit var fileList: ArrayList<String>
    private var padre = ""

    private lateinit var viewLocal: View

    private lateinit var externalCacheDir: File

    private lateinit var image: ImageView

    private var player: MediaPlayer? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        viewLocal = inflater.inflate(R.layout.fragment_reconocer_voz, container, false)

        externalCacheDir = activity!!.externalCacheDir

        image = viewLocal.findViewById(R.id.sinGrabaciones)

        fileList = ArrayList()

        initializeView()

        val recordButton = viewLocal.findViewById<com.getbase.floatingactionbutton.FloatingActionButton>(R.id.recordButton)
        val deleteButton = viewLocal.findViewById<com.getbase.floatingactionbutton.FloatingActionButton>(R.id.delete)
        val finishButton = viewLocal.findViewById<com.getbase.floatingactionbutton.FloatingActionButton>(R.id.finish)
        deleteButton.visibility = View.INVISIBLE
        finishButton.visibility = View.INVISIBLE

        recordButton.setOnClickListener {
            deleteButton.visibility = View.VISIBLE
            finishButton.visibility = View.VISIBLE

            // Record to the external cache directory for visibility

            if (bool) {

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    pauseRecord(recording)
                } else {
                    makeDialog("Función no disponible", "Esta función no está disponible en tu celular")
                }
            } else {
                startRecording()
                bool = true
            }
        }

        finishButton.setOnClickListener {
            stopRecording(true)
            deleteButton.visibility = View.INVISIBLE
            finishButton.visibility = View.INVISIBLE
        }

        deleteButton.setOnClickListener {
            stopRecording(false)
            deleteButton.visibility = View.INVISIBLE
            finishButton.visibility = View.INVISIBLE
        }

        return viewLocal
    }

    @RequiresApi(Build.VERSION_CODES.N)
    private fun pauseRecord(recording: Boolean) {
        if (recording) {
            recordButton.setIcon(R.drawable.ic_mic)
            recorder!!.pause()
        } else {
            recordButton.setIcon(R.drawable.ic_pause_white)
            recorder!!.resume()
        }
        this.recording = !this.recording
    }

    private fun startRecording() {
        recording = true

        val calendar = Calendar.getInstance()

        val hour = calendar.get(Calendar.HOUR_OF_DAY)
        val minute = calendar.get(Calendar.MINUTE)
        val second = calendar.get(Calendar.SECOND)
        val date = calendar.get(Calendar.DATE)

        fileName = "${externalCacheDir.absolutePath}/BHelp$date-$hour:$minute:$second.3gp"

        recordButton.setIcon(R.drawable.ic_pause_white)
        recorder = MediaRecorder().apply {
            setAudioSource(MediaRecorder.AudioSource.MIC)
            setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP)
            setOutputFile(fileName)
            setAudioEncoder(MediaRecorder.AudioEncoder.AMR_WB)

            try {
                prepare()
            } catch (e: IOException) {
            }

            start()
        }
    }

    @SuppressLint("ResourceType")
    private fun stopRecording(save: Boolean) {
        recording = false
        bool = false
        recordButton.setIcon(R.drawable.ic_mic)
        recorder?.apply {
            stop()
            release()
        }

        if (!save) {
            File(fileName).delete()
            recording = !recording
        } else {
            val dialog = Dialog(activity)
            dialog.setContentView(R.layout.dialog_guardar_grabacion)
            dialog.setCancelable(true)

            val text = dialog.findViewById<EditText>(R.id.nombreGrabacion)
            val rdPapa = dialog.findViewById<RadioButton>(R.id.papa)
            val rdMama = dialog.findViewById<RadioButton>(R.id.mama)
            val cancelar = dialog.findViewById<Button>(R.id.guardarDefault)
            val guardar = dialog.findViewById<Button>(R.id.guardarGrabacion)

            rdPapa.setOnClickListener {
                padre = "Papá"
            }
            rdMama.setOnClickListener {
                padre = "Mamá"
            }

            cancelar.setOnClickListener {
                File(fileName).delete()
                dialog.dismiss()
            }
            guardar.setOnClickListener {
                guardarGrabacion("${text.text};$padre")
                dialog.dismiss()
            }
            dialog.window.setBackgroundDrawableResource(R.drawable.round_background)

            dialog.setCancelable(false)

            dialog.show()
        }
        recorder = null
    }

    fun guardarGrabacion(nombre: String) {
        val from = File(fileName)
        val to = File("${externalCacheDir.absolutePath}/$nombre.3gp")
        from.renameTo(to)
        initializeView()
    }

    override fun onStop() {
        super.onStop()
        recorder?.release()
        recorder = null
        player?.release()
        player = null
    }

    private fun listFiles() {
        val path = externalCacheDir.absolutePath
        val directory = File(path)
        val files = directory.listFiles()
        fileList = ArrayList()
        if (files != null && files.isNotEmpty()) {
            for (i in files.indices) {
                val nameParts = files[i].name.split("/")
                val nameParts2 = nameParts[nameParts.size - 1].split(".")
                fileList.add(nameParts2[0])
            }
        }
    }

    private fun initializeView() {

        audioView = viewLocal.findViewById(R.id.audiosView)
        audioView.layoutManager = GridLayoutManager(activity, 1)

        listFiles()

        if (fileList.isEmpty()) {
            image.visibility = View.VISIBLE
        } else {
            image.visibility = View.GONE
        }

        fileList.sort()

        audioListAdapter = AudioListAdapter(this, externalCacheDir.absolutePath, fileList, R.layout.item_audio)
        audioView.adapter = audioListAdapter
    }

    fun makeDialog(titulo:String, mensaje: String){
        val builder = AlertDialog.Builder(context)
        builder.setTitle(titulo)
        builder.setMessage(mensaje)
        builder.setPositiveButton("Aceptar") { _, _ ->
        }
        val dialog = builder.create()
        dialog.show()
    }

}
