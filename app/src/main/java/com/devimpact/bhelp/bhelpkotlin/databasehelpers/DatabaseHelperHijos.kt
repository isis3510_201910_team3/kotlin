package com.devimpact.bhelp.bhelpkotlin.databasehelpers

import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import com.devimpact.bhelp.bhelpkotlin.models.Hijo

class DatabaseHelperHijos(context: Context) : SQLiteOpenHelper(context, TABLE_NAME, null, 1) {

    /**
     * Returns all the data from database
     * @return
     */
    val data: Cursor
        get() {
            val db = this.writableDatabase
            val query = "SELECT * FROM $TABLE_NAME"
            return db.rawQuery(query, null)
        }

    fun getDatos(): Cursor {
        val db = this.writableDatabase
        val query = "SELECT * FROM $TABLE_NAME"
        return db.rawQuery(query, null)
    }

    override fun onCreate(db: SQLiteDatabase) {
        val createTable = "CREATE TABLE " + TABLE_NAME + " (ID TEXT PRIMARY KEY, " +
                COL2 + " TEXT," + COL3 + " INTEGER," + COL4 + " INTEGER," + COL5 + " INTEGER," + COL6 + " INTEGER," + COL7 + " INTEGER, "+ COL8 + " INTEGER, " + COL9 + " TEXT)"
        db.execSQL(createTable)
    }

    override fun onUpgrade(db: SQLiteDatabase, i: Int, i1: Int) {
        db.execSQL("DROP TABLE IF EXISTS $TABLE_NAME")
        onCreate(db)
    }


    fun addData(id:String, hijo: Hijo): Boolean {
        val db = this.writableDatabase
        val contentValues = ContentValues()
        contentValues.put(COL1, id)
        contentValues.put(COL2, hijo.nombre)
        contentValues.put(COL3, hijo.sexo)
        contentValues.put(COL4, hijo.peso)
        contentValues.put(COL5, hijo.edad)
        contentValues.put(COL6, hijo.bebe)
        contentValues.put(COL7, hijo.altura)
        contentValues.put(COL8, hijo.compartido)
        contentValues.put(COL9, hijo.fechaNacimiento)

        val result = db.insert(TABLE_NAME, null, contentValues)

        //if date as inserted incorrectly it will return -1
        return (result !== -1L)
    }

    /**
     * Updates the name field
     * @param nuevoHijo
     * @param id
     */
    fun updateHijo(id:String, nuevoHijo: Hijo) {
        val db = this.writableDatabase
        val query = "UPDATE " + TABLE_NAME + " SET " + COL2 +
                " = '" + nuevoHijo.nombre + "', " + COL3 +
                " = '" + nuevoHijo.sexo + "', " + COL4 +
                " = '" + nuevoHijo.peso + "', " + COL5 +
                " = '" + nuevoHijo.edad + "', " + COL6 +
                " = '" + nuevoHijo.bebe + "', " + COL7 +
                " = '" + nuevoHijo.compartido + "', " + COL8 +
                " = '" + nuevoHijo.fechaNacimiento + "', " + COL9 +
                " = '" + nuevoHijo.altura + "' WHERE " + COL1 + " = '" + id +"'"
        db.execSQL(query)
    }

    /**
     * Delete from database
     * @param id
     * @param name
     */
    fun deleteName(id: String) {
        val db = this.writableDatabase
        val query = ("DELETE FROM " + TABLE_NAME + " WHERE "
                + COL1 + " = '" + id + "'")
        db.execSQL(query)
    }

    companion object {

        private val TABLE_NAME = "hijos_table"
        private val COL1 = "ID"
        private val COL2 = "nombre"
        private val COL3 = "sexo"
        private val COL4 = "peso"
        private val COL5 = "edad"
        private val COL6 = "bebe"
        private val COL7 = "altura"
        private val COL8 = "compartido"
        private val COL9 = "fecha"

    }
}

