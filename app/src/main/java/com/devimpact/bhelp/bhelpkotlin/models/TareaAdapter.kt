package com.devimpact.bhelp.bhelpkotlin.models

class TareaAdapter(val id:String, val titulo:String, val fecha: String, val prioridad: String, val categoria: String, val compartida: Int, val descripcion: String, val hijo:String): Comparable<TareaAdapter>  {

    constructor() : this("", "","", "", "", 0, "", "")

    override fun compareTo(other: TareaAdapter): Int {
        return this.categoria.compareTo(other.categoria)

    }
}