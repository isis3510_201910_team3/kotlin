package com.devimpact.bhelp.bhelpkotlin.fragments.logic

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentTransaction
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.devimpact.bhelp.bhelpkotlin.adapters.TaskListAdapter
import com.devimpact.bhelp.bhelpkotlin.databasehelpers.DatabaseHelper
import com.devimpact.bhelp.bhelpkotlin.databasehelpers.DatabaseHelperHijos
import com.devimpact.bhelp.bhelpkotlin.dialogs.FullScreenDialogCrearHijo
import com.devimpact.bhelp.bhelpkotlin.services.NetworkUtil
import com.devimpact.bhelp.bhelpkotlin.R
//import com.devimpact.bhelp.bhelpkotlin.R.id.sinPrioritarias
import com.devimpact.bhelp.bhelpkotlin.models.TareaAdapter
import com.google.firebase.firestore.FirebaseFirestore
import java.util.*

class FragmentProfileHome : Fragment() {

    private val db by lazy { FirebaseFirestore.getInstance() }
    private lateinit var tareaView: RecyclerView
    private lateinit var tareaListAdapter: TaskListAdapter
    private lateinit var tareasList: ArrayList<TareaAdapter>

    private lateinit var imageAvatar: ImageView
    private lateinit var tituloDB: TextView
    private lateinit var nombreHijo: TextView
    private lateinit var pesoHijo: TextView
    private lateinit var alturaHijo: TextView
    private lateinit var pesoHijoLbl: TextView
    private lateinit var alturaHijoLbl: TextView
    private lateinit var thisView: View

    lateinit var correoUsuario: String
    lateinit var nombre: String
    private lateinit var peso: String
    private lateinit var altura: String
    lateinit var id: String
    private var hayHijo: Boolean = false
    private var bebe = -1
    var sexo = -1
    private lateinit var shared: SharedPreferences

    private var mDatabaseHelperHijos: DatabaseHelperHijos? = null
    var mDatabaseHelper: DatabaseHelper? = null
    private lateinit var supportFragmentManager: FragmentManager

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_profile_home, container, false)
        thisView = view
        tareasList = ArrayList()
        tareaView = view.findViewById(R.id.mostImportantTasks)
        tareaView.layoutManager = GridLayoutManager(this.activity, 1)

        supportFragmentManager = this.activity!!.supportFragmentManager


        imageAvatar = view.findViewById(R.id.avatar_home)
        tituloDB = view.findViewById(R.id.tituloDB)
        nombreHijo = view.findViewById(R.id.nombreHijoDB)
        pesoHijo = view.findViewById(R.id.pesoHijoDB)
        alturaHijo = view.findViewById(R.id.alturaHijoDB)
        pesoHijoLbl = view.findViewById(R.id.pesoHijoDB_label)
        alturaHijoLbl = view.findViewById(R.id.alturaHijoDB_label)

        shared = activity!!.getSharedPreferences(getString(R.string.preference_file_key), Context.MODE_PRIVATE)
        correoUsuario = shared.getString("correoUsuario", "")

        mDatabaseHelperHijos = DatabaseHelperHijos(view.context)
        mDatabaseHelper = DatabaseHelper(view.context)

        tareaListAdapter = TaskListAdapter(this, tareasList, R.layout.item_tarea, correoUsuario)
        tareaView.adapter = tareaListAdapter


        refresh()
        updateView(hayHijo)



        if (NetworkUtil.getConnectivityStatus(view.context) == 0) {
            cargarDatosSQLite()
        } else {
            cargarDatosFirebase()
        }

        return view
    }


    private fun cargarDatosSQLite() {

        val data = mDatabaseHelper!!.getTareasXPrioridad("Alta")
        //val data = mDatabaseHelper!!.data
        while (data!!.moveToNext()) {
            //get the value from the database in column 1
            //then add it to the ArrayList
            tareasList.add(TareaAdapter(data.getString(0), data.getString(1), "${data.getString(4)};${data.getString(5)}", data.getString(3), data.getString(7), data.getString(6).toInt(), data.getString(2),""))
        }
        tareasList.sort()
        //create the list adapter and set the adapter
        tareaListAdapter = TaskListAdapter(this, tareasList, R.layout.item_tarea, correoUsuario)
        tareaView.adapter = tareaListAdapter
    }

    fun cargarDatosFirebase() {

        db.collection("tutores").document(correoUsuario)
                .collection("hijos").document(id)
                .collection("tareasPrioridadAlta")
                .get()
                .addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        for (document in task.result!!) {
                            tareasList.add(document.toObject(TareaAdapter::class.java))
                        }
                        tareasList.sort()
                        tareaListAdapter = TaskListAdapter(this, tareasList, R.layout.item_tarea, correoUsuario)
                        tareaView.adapter = tareaListAdapter
                    }
                }
                .addOnFailureListener { e ->

                }
    }


    private fun cargarHijoSQLite() {
        val data = mDatabaseHelperHijos!!.getDatos()

        if (data!!.moveToNext()) {
            hayHijo = true
            nombreHijo.text = data.getString(1)
            val sexo = Integer.parseInt(data.getString(2))
            pesoHijo.text = data.getString(3)
            val bebe = Integer.parseInt(data.getString(5))
            alturaHijo.text = data.getString(6)

            if (bebe == 1) {
                imageAvatar.setBackgroundResource(R.drawable.ic_baby)
            } else if (sexo == 1 && bebe == 0) {
                imageAvatar.setBackgroundResource(R.drawable.ic_girl)

            } else {
                imageAvatar.setBackgroundResource(R.drawable.ic_boy)
            }
            updateView(hayHijo)
        } else {
            imageAvatar.visibility = View.GONE
            pesoHijoLbl.visibility = View.GONE
            alturaHijoLbl.visibility = View.GONE
            tituloDB.visibility = View.GONE
            tareaView.visibility = View.GONE
        }
    }

    private fun cargarHijoFB() {
        var primero = db.collection("tutores").document(correoUsuario)
                .collection("hijos")
                .get()
                .addOnCompleteListener { task ->
                    if (task.isSuccessful && task.result!!.documents.isNotEmpty()) {
                        hayHijo = true
                        val primero = task.result!!.documents[0]
                        nombreHijo.text = primero.get("nombre").toString()
                        pesoHijo.text = primero.get("peso").toString()
                        alturaHijo.text = primero.get("altura").toString()

                        if (primero.get("bebe") == 1) {
                            imageAvatar.setBackgroundResource(R.drawable.ic_baby)
                        } else if (primero.get("sexo") == 1 && primero.get("bebe") == 0) {
                            imageAvatar.setBackgroundResource(R.drawable.ic_girl)
                        } else {
                            imageAvatar.setBackgroundResource(R.drawable.ic_boy)

                        }
                        updateView(true)

                    } else {
                        imageAvatar.visibility = View.GONE
                        pesoHijoLbl.visibility = View.GONE
                        alturaHijoLbl.visibility = View.GONE
                        tituloDB.visibility = View.GONE
                        tareaView.visibility = View.GONE
                    }
                }
    }


    private fun updateView(hay: Boolean) {
            imageAvatar.visibility = View.VISIBLE
            pesoHijoLbl.visibility = View.VISIBLE
            tituloDB.visibility = View.VISIBLE
            alturaHijoLbl.visibility = View.VISIBLE
            tareaView.visibility = View.VISIBLE
    }

    fun refresh() {
        hayHijo = true
        nombre = shared.getString("nombre", "")
        nombreHijo.text = nombre

        id = shared.getString("idHijo","")

        peso = shared.getString("peso", "")
        pesoHijo.text = "$peso g"

        altura = shared.getString("altura", "")
        alturaHijo.text = "$altura cm"

        bebe = shared.getInt("bebe", 0)
        sexo = shared.getInt("sexo", 0)

        if (bebe == 1) {
            imageAvatar.setBackgroundResource(R.drawable.ic_baby)
        } else if (sexo == 1 && bebe == 0) {
            imageAvatar.setBackgroundResource(R.drawable.ic_girl)
        } else {
            imageAvatar.setBackgroundResource(R.drawable.ic_boy)
        }
    }

}
