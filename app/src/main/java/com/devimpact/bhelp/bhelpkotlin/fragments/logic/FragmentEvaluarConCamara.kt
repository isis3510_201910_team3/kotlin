package com.devimpact.bhelp.bhelpkotlin.fragments.logic

import android.app.Activity
import android.app.AlertDialog
import android.content.ContentResolver
import android.content.ContentValues
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.*
import android.net.Uri
import android.os.AsyncTask
import android.os.Bundle
import android.provider.MediaStore
import android.support.v4.app.Fragment
import android.text.InputType
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.ImageView
import com.devimpact.bhelp.bhelpkotlin.R
import com.devimpact.bhelp.bhelpkotlin.activity.Menu
import com.devimpact.bhelp.bhelpkotlin.asynctask.ImageCircleRecognitionAsyncTask
import com.theartofdev.edmodo.cropper.CropImage
import org.opencv.android.OpenCVLoader
import org.opencv.android.Utils
import org.opencv.core.CvType
import org.opencv.core.Mat
import org.opencv.core.Scalar
import org.opencv.core.Size
import org.opencv.core.Point
import org.opencv.imgproc.Imgproc


class FragmentEvaluarConCamara : Fragment() {

    private val TAKE_PHOTO_REQUEST: Int = 2
    private val PICK_PHOTO_REQUEST: Int = 1

    private var fileUri: Uri? = null

    private var width = 0
    private var height = 0

    private var bitmap: Bitmap? = null

    private lateinit var contentResolver: ContentResolver
    private lateinit var packageManager: PackageManager

    private lateinit var imageView: ImageView


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_evaluar_crecimiento, container, false)

        imageView = view.findViewById(R.id.imageViewCrecimiento)

        val camera = view.findViewById<com.getbase.floatingactionbutton.FloatingActionButton>(R.id.camera)
        camera.setOnClickListener {
            launchCamera()
        }

        val gallery = view.findViewById<com.getbase.floatingactionbutton.FloatingActionButton>(R.id.gallery)
        gallery.setOnClickListener {
            pickPhotoFromGallery()
        }


        contentResolver = activity!!.contentResolver
        packageManager = activity!!.packageManager

        return view
    }

    private fun pickPhotoFromGallery() {
        val pickImageIntent = Intent(Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI)

        startActivityForResult(pickImageIntent, PICK_PHOTO_REQUEST)
    }

    //launch the camera to take photo via intent
    private fun launchCamera() {
        val values = ContentValues(1)
        values.put(MediaStore.Images.Media.MIME_TYPE, "image/jpg")
        fileUri = contentResolver
                .insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                        values)
        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        if (intent.resolveActivity(packageManager) != null) {
            intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri)
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION
                    or Intent.FLAG_GRANT_WRITE_URI_PERMISSION)
            startActivityForResult(intent, TAKE_PHOTO_REQUEST)
        }
    }


    //override function that is called once the photo has been taken
    override fun onActivityResult(requestCode: Int, resultCode: Int,
                                  data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK
                && requestCode == TAKE_PHOTO_REQUEST) {
            //photo from camera
            //display the photo on the imageview
            var bitmapAux = MediaStore.Images.Media.getBitmap(this.contentResolver, fileUri)
            CropImage.activity(fileUri).start(context!!, this)

        } else if (resultCode == Activity.RESULT_OK
                && requestCode == PICK_PHOTO_REQUEST) {
            //photo from gallery
            fileUri = data?.data
            CropImage.activity(fileUri).start(context!!, this)
        } else if (resultCode == Activity.RESULT_OK && requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {


            val result = CropImage.getActivityResult(data)

            val bitmapAux = MediaStore.Images.Media.getBitmap(this.contentResolver, result.uri)
            width = bitmapAux.width
            height = bitmapAux.height
            imageView.setImageBitmap(bitmapAux)
            bitmap = bitmapAux

            var size = 0.0

            //Dialog for input
            val builder = AlertDialog.Builder(activity!!)
            builder.setTitle("Ingresa la medida de la guía")

            //Input
            val input   = EditText(activity!!)
            input.inputType = InputType.TYPE_CLASS_TEXT
            builder.setView(input)

            //Positive Button
            builder.setPositiveButton("OK") { dialog, which ->
                size = input.text.toString().toDouble()
                ImageCircleRecognitionAsyncTask(activity as Menu, bitmapAux, size).execute()
            }

            //builder.show()
            size = 2.4

            bitmap = ImageCircleRecognitionAsyncTask(activity as Menu, bitmapAux, size).execute().get()

            imageView.setImageBitmap(bitmap)
            imageView.setBackgroundResource(R.color.white)

        }
        else {
            super.onActivityResult(requestCode, resultCode, data)
        }

    }

    fun makeDialog(titulo:String, mensaje: String){
        val builder = AlertDialog.Builder(context)
        builder.setTitle(titulo)
        builder.setMessage(mensaje)
        builder.setPositiveButton("Aceptar") { _, _ ->
        }
        val dialog = builder.create()
        dialog.show()
    }

}
