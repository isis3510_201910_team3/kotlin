package com.devimpact.bhelp.bhelpkotlin.adapters

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import com.devimpact.bhelp.bhelpkotlin.fragments.logic.*

class AdapterFragmentTabsInformacion(fm: FragmentManager, correoUsuario: String) : FragmentPagerAdapter(fm) {

    override fun getItem(position: Int): Fragment {
        return when (position) {
            0 -> FragmentCuidado()
            1 -> FragmentCrecimiento()
            else -> {
                FragmentSalud()
            }
        }
    }

    override fun getCount(): Int {
        return 3
    }

    override fun getPageTitle(position: Int): CharSequence {
        return when (position) {
            0 -> "Cuidado"
            1 -> "Crecimiento"
            else -> {
                return "Salud"
            }
        }
    }
}