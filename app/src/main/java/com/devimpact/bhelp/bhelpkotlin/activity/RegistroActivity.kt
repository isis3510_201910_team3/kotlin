package com.devimpact.bhelp.bhelpkotlin.activity

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.annotation.TargetApi
import android.app.AlertDialog
import android.app.DatePickerDialog
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.net.ConnectivityManager
import android.os.Build
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.text.TextUtils
import android.util.Log
import android.view.View
import android.widget.*
import com.devimpact.bhelp.bhelpkotlin.R
import com.google.firebase.auth.AuthResult
import kotlinx.android.synthetic.main.activity_registro.*
import java.text.SimpleDateFormat
import com.google.firebase.auth.FirebaseAuth
import java.util.*
import android.widget.EditText
import com.devimpact.bhelp.bhelpkotlin.models.Tutor
import com.devimpact.bhelp.bhelpkotlin.services.NetworkBroadcastReceiver
import com.google.android.gms.tasks.Task
import com.google.firebase.firestore.FirebaseFirestore

class RegistroActivity : AppCompatActivity() {

    private lateinit var editNombre: EditText
    private lateinit var editCorreo: EditText
    private lateinit var editContra: EditText
    private var textFecha: TextView? = null
    private lateinit var editFecha: TextView
    private lateinit var editHijos: EditText
    private lateinit var rdbGeneroM: RadioButton
    private lateinit var rdbGeneroF: RadioButton
    private lateinit var editEPS: EditText
    private lateinit var editEstrato: EditText
    private lateinit var broadcastReceiver: NetworkBroadcastReceiver

    private lateinit var firebaseAuth:FirebaseAuth

    private lateinit var tutor: Tutor

    private var genero = 1
    private var edad = 0
    private var email = ""
    private var password = ""
    private var eps = ""
    private var estrato = ""
    private var anioNacimiento = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_registro)

        installListener()

        firebaseAuth = FirebaseAuth.getInstance()

        editNombre = findViewById(R.id.nombre_completo)
        editCorreo = findViewById(R.id.email)
        editContra = findViewById(R.id.password)

        textFecha = findViewById(R.id.fechaCumple_text)
        editFecha = findViewById(R.id.fechaCumple_text)

        editHijos = findViewById(R.id.hijos)

        rdbGeneroF = findViewById(R.id.femenino)
        rdbGeneroM = findViewById(R.id.masculino)

        editEPS = findViewById(R.id.eps)
        editEstrato = findViewById(R.id.estrato)


        val c = Calendar.getInstance()
        anioNacimiento = c.get(Calendar.YEAR)

        rdbGeneroF.setOnClickListener {
            genero = 1
        }
        rdbGeneroM.setOnClickListener {
            genero = 0
        }

        textFecha!!.setOnClickListener { showDatePickerDialog() }
        registro_button.setOnClickListener {
            if(validateFields()) {
                showProgress(true)
                if( guardarUsuarioFirebase()){
                   registrarUsuario()
               }
            }
        }
    }

    private fun registrarUsuario() {

       firebaseAuth!!.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener { task: Task<AuthResult> ->
                    if (task.isSuccessful) {

                        abrirPrincipal(true)
                    } else {
                        makeDialog("Error registrando usuario", "No se pudo registrar el usuario. Vuelve a intentarlo más tarde")
                    }
                }
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private fun showProgress(show: Boolean) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            val shortAnimTime = resources.getInteger(android.R.integer.config_shortAnimTime).toLong()

            registro_form.visibility = if (show) View.GONE else View.VISIBLE
            registro_form.animate()
                    .setDuration(shortAnimTime)
                    .alpha((if (show) 0 else 1).toFloat())
                    .setListener(object : AnimatorListenerAdapter() {
                        override fun onAnimationEnd(animation: Animator) {
                            registro_form.visibility = if (show) View.GONE else View.VISIBLE
                        }
                    })

            registro_progress.visibility = if (show) View.VISIBLE else View.GONE
            registro_progress.animate()
                    .setDuration(shortAnimTime)
                    .alpha((if (show) 1 else 0).toFloat())
                    .setListener(object : AnimatorListenerAdapter() {
                        override fun onAnimationEnd(animation: Animator) {
                            registro_progress.visibility = if (show) View.VISIBLE else View.GONE
                        }
                    })
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            registro_progress.visibility = if (show) View.VISIBLE else View.GONE
            registro_form.visibility = if (show) View.GONE else View.VISIBLE
        }
    }

    private fun abrirPrincipal(registro:Boolean)
    {
        if(registro) {
            val pos = email.indexOf("@")
            val user = email.substring(0, pos)

            val principal = Intent(this, Menu::class.java)
            val sharedPref = getSharedPreferences(getString(R.string.preference_file_key), Context.MODE_PRIVATE)
            sharedPref.edit().putString("correoUsuario", email).commit()
            finish()
            startActivity(principal)
        }
    }

    private fun guardarUsuarioFirebase(): Boolean {
        var rta = true
        val rootRef = FirebaseFirestore.getInstance()
        val ref = rootRef
                .collection("tutores")
        ref.document(email).set(tutor)
                .addOnFailureListener { e ->
                    makeDialog("Error registrando usuario", "No se pudo registrar el usuario ")

                        rta = false
                }
        return rta
    }

    private fun showDatePickerDialog() {
        val c = Calendar.getInstance()
        val year = c.get(Calendar.YEAR)
        val month = c.get(Calendar.MONTH)
        val day = c.get(Calendar.DAY_OF_MONTH)

        val dpd = DatePickerDialog(this, DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->

            c.set(year, monthOfYear, dayOfMonth)
            anioNacimiento = year
            val format = SimpleDateFormat("dd-MM-yyyy", Locale.US)
            textFecha!!.text = format.format(c.time)

        }, year, month, day)

        dpd.datePicker.maxDate = c.timeInMillis
        dpd.show()
    }

    private fun isEmailValid(email: String): Boolean {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()
    }


    private fun validateFields():Boolean {
        val c = Calendar.getInstance()
        val year = c.get(Calendar.YEAR)

        email = editCorreo!!.text.toString().trim { it <= ' ' }
        password = editContra!!.text.toString().trim { it <= ' ' }

        val nombre = editNombre.text.toString().trim()
        val fecha = editFecha.text.toString().trim()
        val hijos = editHijos.text.toString().trim()
        eps = editEPS.text.toString().trim()
        estrato = editEstrato.text.toString().trim()

        val regex = Regex(pattern = "a|b|c|d|e|f|g|h|i|j|k|l|m|n|ñ|o|p|q|r|s|t|v|w|x|y|z|A|B|C|D|E|F|G|H|I|J|K|L|M|N|Ñ|O|P|Q|R|S|T|U|V|W|X|Y|Z")

        if (TextUtils.isEmpty(nombre) || nombre.isEmpty()) {
            editNombre.error = "Por favor ingresa tu nombre"
            return false
        }

        if (nombre.length !in 5..25) {
            editNombre.error = "El nombre debe tener entre 5 y 25 caracteres"
            return false
        }
        if(!regex.containsMatchIn(input = ""+nombre) ){
            editNombre.error = "El título no debe tener caracteres especiales"
            return false
        }

        if (TextUtils.isEmpty(email)) {
            editCorreo.error = "Se debe ingresar un email"
            return false
        }

        if (email.length !in 5..30) {
            editCorreo.error = "El correo debe tener entre 5 y 25 caracteres"
            return false
        }

        val pos = email.indexOf("@")
        val correo = email.substring(pos, email.length)

        if(!email.contains("@") && isEmailValid(correo))
        {
            editCorreo.error = "Se debe ingresar un email válido(@gmail, @homail, etc)"
            return false
        }

        if (TextUtils.isEmpty(password)) {
            editContra.error = "Falta ingresar la contraseña"
            return false
        }

        if(password.length < 6){
            editContra.error = "La contraseña debe tener mínimo 6 caracteres"
            return false
        }

        if (password.length !in 6..20) {
            editContra.error = "La contraseña debe tener entre 6 y 20 caracteres"
            return false
        }

        edad = year.minus(anioNacimiento)

        if(edad == 0){
            textFecha!!.error = "Por favor, ingresa tu fecha de nacimiento"
            return false
        }

        if(edad < 14){
            textFecha!!.error = "Debes tener almenos 14 años para poder registrarte"
            return false
        }

        if (TextUtils.isEmpty(hijos) || hijos.isEmpty()) {
            editHijos.error = "Por favor informanos cuántos hijos tienes"
            return false
        }

        if (hijos.length !in 1..2) {
            editHijos.error = "Verficia la cantidad de hijos"
            return false
        }

        if (eps.length > 20) {
            editEPS.error = "El nombre de la eps debe ser de máximo 20 caracteres"
            return false
        }

        if (estrato != "" && estrato.toInt() > 10) {
            editEstrato.error = "Estrato inválido"
            return false
        }

        tutor = Tutor(email, nombre, email, edad, genero, estrato )
        return true
    }

    private fun installListener() {
        broadcastReceiver = NetworkBroadcastReceiver()
        registerReceiver(broadcastReceiver, IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION))
    }

    override fun onDestroy() {
        super.onDestroy()
        unregisterReceiver(broadcastReceiver)
    }

    fun makeDialog(titulo:String, mensaje: String){
        val builder = AlertDialog.Builder(this)
        builder.setTitle(titulo)
        builder.setMessage(mensaje)
        builder.setPositiveButton("Aceptar") { _, _ ->
        }
        val dialog = builder.create()
        dialog.show()
    }
}


