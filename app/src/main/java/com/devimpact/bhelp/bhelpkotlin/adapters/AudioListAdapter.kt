package com.devimpact.bhelp.bhelpkotlin.adapters

import android.media.MediaPlayer
import android.support.v4.content.ContextCompat
import android.support.v4.graphics.drawable.DrawableCompat
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.devimpact.bhelp.bhelpkotlin.fragments.logic.FragmentReconocerVoz
import com.devimpact.bhelp.bhelpkotlin.R
import java.io.FileInputStream
import kotlin.math.round


class AudioListAdapter(reconocerVozFragment: FragmentReconocerVoz, basePath: String, files: ArrayList<String>, audio_item: Int) : RecyclerView.Adapter<AudioListAdapter.SingleItemRowHolder>() {

    private var basePath: String = basePath
    private var fileName: String = ""
    private var playing: Boolean = false
    private var itemsList: ArrayList<String> = files
    private var padre = ""
    var layout: Int = audio_item
    private lateinit var thread: Thread
    var progress = 0
    private var first = true
    private var pause = true

    private var player: MediaPlayer? = MediaPlayer()


    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): SingleItemRowHolder {
        val v = LayoutInflater.from(viewGroup.context).inflate(layout, null)
        thread = Thread()
        return SingleItemRowHolder(v)
    }

    override fun onBindViewHolder(holder: SingleItemRowHolder, i: Int) {

        if (itemsList.isNotEmpty()) {
            val item = itemsList[i]

            val parts = item.split(";")
            holder.audio.tag = i
            holder.audio.text = parts[0]
            if (parts.size == 2) {
                holder.localPadre = parts[1]
                padre = parts[1]
                if (padre == "Papá") {
                    holder.image.setBackgroundResource(R.drawable.ic_boy)
                } else {
                    holder.image.setBackgroundResource(R.drawable.ic_girl)
                }
            }



            val file = FileInputStream("$basePath/$item.3gp")

            val mPlayer = MediaPlayer()
            mPlayer.setDataSource(file.fd)
            mPlayer.prepare()

            val duration = round(mPlayer.duration / 1000.0).toInt()

            if (duration < 10) {
                holder.hint.text = "0:0$duration"
            } else {
                holder.hint.text = "0:$duration"
            }

            mPlayer.release()

            holder.seekBar.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {

                override fun onProgressChanged(seekBar: SeekBar, progress: Int, fromTouch: Boolean) {
                    // Display the current progress of SeekBar
                    val x = Math.ceil(progress / 1000.0).toInt()
                    if (x < 10) {
                        holder.hint.text = "0:0$x"
                    } else {
                        holder.hint.text = "0:$x"
                    }
                    if (x == 0) {
                        if (duration < 10) {
                            holder.hint.text = "0:0$duration"
                        } else {
                            holder.hint.text = "0:$duration"
                        }
                    }
                }

                override fun onStartTrackingTouch(seekBar: SeekBar) {
                    // Do something
                    holder.hint.text = duration.toString()
                }

                override fun onStopTrackingTouch(seekBar: SeekBar) {
                    // Do something
                    if (player != null && player!!.isPlaying) {
                        player!!.seekTo(seekBar.progress)
                    } else{
                        progress = seekBar.progress
                    }
                }
            })
        }
    }

    override fun getItemCount(): Int {
        return itemsList.size
    }

    inner class SingleItemRowHolder(view: View) : RecyclerView.ViewHolder(view) {

        var localPadre = ""
        var audio: TextView = view.findViewById(R.id.audioName)
        var image: ImageView = view.findViewById(R.id.imagenGrabacion)
        var playButton: ImageButton = view.findViewById(R.id.playButton)
        var seekBar: SeekBar = view.findViewById(R.id.seekbar)
        var hint: TextView = view.findViewById(R.id.duration)

        init {
            if (localPadre != "") {
                if (localPadre == "Papá") {
                    image.setBackgroundResource(R.drawable.ic_boy)
                } else {
                    image.setBackgroundResource(R.drawable.ic_girl)
                }
            }

            playButton.setOnClickListener { v ->
                fileName = "$basePath/${audio.text};$localPadre.3gp"

                playing = !playing

                onPlay(playing, v, seekBar, playButton)
            }
        }
    }

    private fun onPlay(start: Boolean, v: View, seekBar: SeekBar, playButton: ImageButton) = if (start) {
        startPlaying(v, seekBar, playButton)
    } else {
        progress = seekBar.progress
        stopPlaying(playButton, seekBar)
    }

    private fun startPlaying(v: View, seekBar: SeekBar, playButton: ImageButton) {
        pause = false
        playButton.setBackgroundResource(R.drawable.ic_pause)
        DrawableCompat.setTint(playButton.background, ContextCompat.getColor(v.context, R.color.colorSecondary))

        if(fileName.endsWith(";.3gp"))
            fileName = fileName.replace(";","")

        val file = FileInputStream(fileName)

        if ((player != null && player!!.isPlaying) || thread.state.toString() == "TERMINATED" || first) {
            first = false
            player!!.stop()
            player = MediaPlayer()
            thread.interrupt()
            thread = Thread {
                var currentPosition = player!!.currentPosition
                val total = player!!.duration


                while (player != null && player!!.isPlaying && currentPosition < total) {
                    try {
                        Thread.sleep(100)
                        currentPosition = player!!.currentPosition
                    } catch (e: InterruptedException) {
                    } catch (e: Exception) {
                    }
                    seekBar.progress = currentPosition
                }
                if(!pause) {
                    playing = false
                    seekBar.progress = 0
                    progress = 0
                }
            }
        }

        player!!.setDataSource(file.fd)
        player!!.prepare()
        seekBar.max = player!!.duration
        player!!.seekTo(progress)
        player!!.start()
        thread.start()

        player!!.setOnCompletionListener {
            player!!.reset()
            playButton.setBackgroundResource(R.drawable.ic_play)
        }
    }

    private fun stopPlaying(playButton: ImageButton, seekBar: SeekBar) {
        progress = seekBar.progress
        player!!.pause()
        pause = true
        playButton.setBackgroundResource(R.drawable.ic_play)
    }
}