package com.devimpact.bhelp.bhelpkotlin.asynctask

import android.app.AlertDialog
import android.graphics.Bitmap
import android.os.AsyncTask
import com.devimpact.bhelp.bhelpkotlin.activity.Menu
import org.opencv.android.Utils
import org.opencv.core.*
import org.opencv.imgproc.Imgproc
import java.lang.ref.WeakReference
import org.opencv.core.Mat
import org.opencv.android.OpenCVLoader






class ImageCircleRecognitionAsyncTask(context: Menu, bit: Bitmap, size:Double): AsyncTask<Int, Void, Bitmap>() {

    private val UNDEFINED = -1
    private val SUCCESS = 0
    private val MORE = 1
    private val NONE = 2

    private var size = size


    private var tamanio = ""
    private var numberOfCircles = 0
    private var bitmap = bit
    private var state = UNDEFINED
    private val activityReference: WeakReference<Menu> = WeakReference(context)

    init
    {
        if (OpenCVLoader.initDebug()) {
            //Handle
        }
    }

    override fun onPreExecute() {
        val activity = activityReference.get()
        if (activity == null || activity.isFinishing) return
    }

    override fun doInBackground(vararg params: Int?): Bitmap? {
        /* convert bitmap to mat */
        val mat = Mat(bitmap.width, bitmap.height,
                CvType.CV_8UC1)
        val grayMat = Mat(bitmap.width, bitmap.height,
                CvType.CV_8UC1)

        Utils.bitmapToMat(bitmap, mat)

/* convert to grayscale */
        val colorChannels = if (mat.channels() === 3)
            Imgproc.COLOR_BGR2GRAY
        else
            if (mat.channels() === 4) Imgproc.COLOR_BGRA2GRAY else 1

        Imgproc.cvtColor(mat, grayMat, colorChannels)

/* reduce the noise so we avoid false circle detection */
        Imgproc.GaussianBlur(grayMat, grayMat, Size(9.0, 9.0), 2.0, 2.0)

// accumulator value
        val dp = 1.2
// minimum distance between the center coordinates of detected circles in pixels
        val minDist = 100.0

// min and max radii (set these values as you desire)
        val minRadius = 0
        val maxRadius = 0

// param1 = gradient value used to handle edge detection
// param2 = Accumulator threshold value for the
// cv2.CV_HOUGH_GRADIENT method.
// The smaller the threshold is, the more circles will be
// detected (including false circles).
// The larger the threshold is, the more circles will
// potentially be returned.
        val param1 = 170.0
        val param2 = 74.0

/* create a Mat object to store the circles detected */
        val circles = Mat(bitmap.width,
                bitmap.height, CvType.CV_8UC1)

/* find the circle in the image */
        Imgproc.HoughCircles(grayMat, circles,
                Imgproc.CV_HOUGH_GRADIENT, dp, minDist, param1,
                param2, minRadius, maxRadius)



/* get the number of circles detected */
        numberOfCircles = if (circles.rows() === 0) 0 else circles.cols()
        // circle detected coordinates
        val circleCoordinates = circles.get(0, 0)
        if (numberOfCircles == 1 && circleCoordinates.size == 3) {
            val x = circleCoordinates[0]
            val y = circleCoordinates[1]

            val center = org.opencv.core.Point(x, y)

            val radius = circleCoordinates[2].toInt()

            var finalPositionTop = y - circleCoordinates[2]*2
            var countCircles = 0.0
            var finalPositionBottom = y + circleCoordinates[2]*2
            val landMarkPosition = y
            val landmarkSize = 2.4

            //Se calcula cuantos circulos más caben hacia arriba
            while(finalPositionTop >= (0 - radius))
            {
                val newCenter = Point(x, finalPositionTop)
//                Imgproc.circle(mat, newCenter, radius, Scalar(0.0,
//                        255.0, 0.0), 4)
                finalPositionTop -= radius*2
            }

            countCircles += (landMarkPosition + radius) / (radius*2)

            while(finalPositionBottom <= (bitmap.height + radius))
            {
                val newCenter = Point(x, finalPositionBottom)
//                Imgproc.circle(mat, newCenter, radius, Scalar(0.0,
//                        255.0, 0.0), 4)
                finalPositionBottom += radius*2
            }

            countCircles += (mat.height() - landMarkPosition - radius) / (radius*2)

            /* circle's outline */
            Imgproc.circle(mat, center, radius, Scalar(0.0,
                    255.0, 0.0), 4)


            /* circle's center outline */
            Imgproc.rectangle(mat, Point(x - 5, y - 5),
                    Point(x + 5, y + 5),
                    Scalar(0.0, 128.0, 255.0), -1)

            tamanio = "%.2f".format(countCircles*size)
            state = SUCCESS


        }
        else if(numberOfCircles > 1) {
            for(i in 0.rangeTo(numberOfCircles))
            {
                val circleCoordinates = circles.get(0, i)
                if(circleCoordinates != null && circleCoordinates.size == 3) {
                    val x = circleCoordinates[0]
                    val y = circleCoordinates[1]

                    val center = org.opencv.core.Point(x, y)

                    val radius = circleCoordinates[2].toInt()

                    Imgproc.circle(mat, center, radius, Scalar(0.0,
                            255.0, 0.0), 4)


                    /* circle's center outline */
                    Imgproc.rectangle(mat, Point(x - 5, y - 5),
                            Point(x + 5, y + 5),
                            Scalar(0.0, 128.0, 255.0), -1)
                }

            }

            state = MORE


        } else {
            state = NONE
        }

/* convert back to bitmap */
        Utils.matToBitmap(mat, bitmap)
        return bitmap
    }


    override fun onPostExecute(result: Bitmap) {

        val activity = activityReference.get()
        if (activity == null || activity.isFinishing) return

        when(state) {
            MORE -> makeDialog("Error de medición", "Identificamos $numberOfCircles guías, por favor vuelve a intentarlo.")
            NONE -> makeDialog("Error de medición", "No pudimos identificar correctamente la guía. Por favor vuelve a intentarlo.")
            SUCCESS -> if(numberOfCircles == 1) makeDialog("¡Tu medida!", "Lo seleccionado mide: $tamanio cm.")
        }
    }


    fun makeDialog(titulo:String, mensaje: String){
        val activity = activityReference.get()
        if (activity == null || activity.isFinishing) return
        val builder = AlertDialog.Builder(activity)
        builder.setTitle(titulo)
        builder.setMessage(mensaje)
        builder.setPositiveButton("Aceptar") { _, _ ->
        }
        val dialog = builder.create()
        dialog.show()
    }
}
