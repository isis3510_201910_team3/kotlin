package com.devimpact.bhelp.bhelpkotlin.fragments.agenda

import android.content.Context
import android.os.Bundle
import android.support.v7.widget.GridLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.devimpact.bhelp.bhelpkotlin.*
import com.devimpact.bhelp.bhelpkotlin.adapters.TaskListAdapter
import com.devimpact.bhelp.bhelpkotlin.databasehelpers.DatabaseHelper
import com.devimpact.bhelp.bhelpkotlin.services.NetworkUtil
import com.google.firebase.firestore.FirebaseFirestore


class FragmentAlimentos : AgendaFragmentInterface() {

    private val db by lazy { FirebaseFirestore.getInstance() }
    private lateinit var tareaListAdapter: TaskListAdapter
    var mDatabaseHelper: DatabaseHelper? = null
    private var refresh = false
    lateinit var correoUsuario: String

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val view = inflater.inflate(R.layout.fragment_alimentos, container, false)
        tareasList = ArrayList()

        mensaje = view!!.findViewById(R.id.txt_sinTareas)
        mensje = view!!.findViewById(R.id.txt_sinTareas2)
        tareaView = view!!.findViewById(R.id.tareasAlimentosView)
        tareaView.layoutManager = GridLayoutManager(this.activity, 1)
        sinTareas = view!!.findViewById(R.id.sinTareas)
        val sharedPref = activity!!.getSharedPreferences(getString(R.string.preference_file_key), Context.MODE_PRIVATE)
        correoUsuario = sharedPref.getString("correoUsuario","")
        mDatabaseHelper = DatabaseHelper(view.context)

        actualizar(true )

        return view
    }


    override fun actualizar(refreshData: Boolean){
        refresh = refreshData
        if(refreshData) {
            tareasList = ArrayList()
            if(NetworkUtil.getConnectivityStatus(activity!!.applicationContext)==0){
                cargarDatos(false,mDatabaseHelper!!,db,tareasList,"Alimentos",refresh, sinTareas, tareaView, mensaje,mensje,correoUsuario)
                refresh = false
            }
            else{
                cargarDatos(true,mDatabaseHelper!!,db,tareasList,"Alimentos",refresh, sinTareas, tareaView,mensaje,mensje,correoUsuario)
                refresh = false
            }
        }

        tareaListAdapter = TaskListAdapter(this, tareasList, R.layout.item_tarea, correoUsuario)
        tareaView.adapter = tareaListAdapter
        registerForContextMenu(tareaView)
    }
}




