package com.devimpact.bhelp.bhelpkotlin.dialogs

import android.app.AlertDialog
import android.app.DatePickerDialog
import android.content.Context
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.text.TextUtils
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.*
import android.widget.EditText
import com.devimpact.bhelp.bhelpkotlin.adapters.RecyclerTouchListener
import com.devimpact.bhelp.bhelpkotlin.adapters.VacunaListAdapter
import com.devimpact.bhelp.bhelpkotlin.databasehelpers.DatabaseHelperHijos
import com.devimpact.bhelp.bhelpkotlin.fragments.logic.FragmentHijos
import com.devimpact.bhelp.bhelpkotlin.services.NetworkUtil
import com.devimpact.bhelp.bhelpkotlin.R
import com.devimpact.bhelp.bhelpkotlin.models.Hijo
import com.devimpact.bhelp.bhelpkotlin.models.Vacuna
import com.google.firebase.firestore.FirebaseFirestore
import java.text.SimpleDateFormat
import java.util.*
import java.util.regex.Pattern
import kotlin.collections.ArrayList


class FullScreenDialogEditHijo : DialogFragment() {

    private val db by lazy { FirebaseFirestore.getInstance() }
    private lateinit var editNombre: EditText
    private lateinit var editPeso: EditText
    private lateinit var editAltura: EditText
    private lateinit var editFecha: TextView
    private lateinit var rdbGeneroM: RadioButton
    private lateinit var rdbGeneroF: RadioButton

    private var genero = 1
    private var edad = 0
    private var anioNacimiento = -1
    private lateinit var buttonGuardar: TextView

    private var mDatabaseHelperHijos: DatabaseHelperHijos? = null
    private lateinit var viejoNombre: String
    private lateinit var viejaEdad: String
    private lateinit var viejoID: String
    private lateinit var viejoPeso: String
    private lateinit var viejoAltura: String
    private var viejoFecha = ""
    private lateinit var hijo: Hijo
    private lateinit var supportFragmentManager: FragmentManager
    private lateinit var fragmentHijos: Fragment
    lateinit var correoUsuario: String
    private lateinit var contextA: Context


    private lateinit var vacunasView: RecyclerView
    private lateinit var vacunasListAdapter: VacunaListAdapter
    private lateinit var vacunasAlDiaList: ArrayList<Vacuna>
    private lateinit var vacunas: ArrayList<Vacuna>

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.dialog_crear_hijo, container, false)
        mDatabaseHelperHijos = DatabaseHelperHijos(activity!!)
        supportFragmentManager = activity!!.supportFragmentManager
        val sharedPref = activity!!.getSharedPreferences(getString(R.string.preference_file_key), Context.MODE_PRIVATE)
        correoUsuario = sharedPref.getString("correoUsuario","")

        val close = view.findViewById<ImageButton>(R.id.ic_close)
        buttonGuardar = view.findViewById(R.id.guardarHijo)
        editNombre = view.findViewById(R.id.nombreHijo)
        editPeso = view.findViewById(R.id.pesoHijo)
        editAltura = view.findViewById(R.id.alturaHijo)

        viejoNombre = arguments!!.getString("nombre")
        editNombre.hint = viejoNombre
        editNombre.setText(viejoNombre)

        viejoPeso = arguments!!.getString("peso")
        editPeso.hint = viejoPeso
        editPeso.setText(viejoPeso)


        viejoAltura = arguments!!.getString("altura")
        editAltura.hint = viejoAltura
        editAltura.setText(viejoAltura)

        viejaEdad = arguments!!.getString("edad")
        contextA = view.context

        viejoID = arguments!!.getString("id")
        editFecha = view.findViewById(R.id.fechaNac_text)
        rdbGeneroF = view.findViewById(R.id.femenino)
        rdbGeneroM = view.findViewById(R.id.masculino)

        viejoFecha = arguments!!.getString("fecha")
        editFecha!!.text = viejoFecha

        vacunasAlDiaList = ArrayList()
        vacunasView = view.findViewById(R.id.vacunas)
        vacunasView.layoutManager = GridLayoutManager(this.activity, 1)


        vacunasView.addOnItemTouchListener(RecyclerTouchListener(
                context!! ,
                vacunasView ,
                object : RecyclerTouchListener.ClickListener
                {
                    override fun onClick(view: View, position: Int) {
                        vacunas[position].aplicada = 1 - vacunas[position].aplicada
                    }
                }))


        cargarDatosFirebase()

        val c = Calendar.getInstance()
        anioNacimiento = c.get(Calendar.YEAR)

        rdbGeneroF.setOnClickListener {
            genero = 1
        }
        rdbGeneroM.setOnClickListener {
            genero = 0
        }


        for(i in supportFragmentManager.fragments.iterator())
        {
            if(i.toString().contains("FragmentHijos") ) {
                fragmentHijos = i
            }
        }

        editFecha!!.setOnClickListener { showDatePickerDialog() }

        close.setOnClickListener {
            dismiss()
        }
        buttonGuardar.setOnClickListener {
            if(validateFields()) {
                if (NetworkUtil.getConnectivityStatus(activity!!) == 0) {
                    makeDialog("Error de conexión", "En este momento no estás conectado a internet, la información se guardará localmente y cuando vuelvas a conectarte la sincronizaremos")
                }
                guardarHijoLocal()
                guardarHijoFirebase()
            }
            val fragment : FragmentHijos = supportFragmentManager.findFragmentByTag("fragmentHijos") as FragmentHijos
            val imm = activity!!.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            if(activity!!.currentFocus != null)
                imm.hideSoftInputFromWindow(activity!!.currentFocus.windowToken,0)
            fragment.actualizar(true)
        }

        return view
    }


    private fun guardarHijoFirebase() {


        val rootRef = FirebaseFirestore.getInstance()
        val ref = rootRef
                .collection("tutores").document(correoUsuario)
                .collection("hijos")
        ref.document(viejoID).set(hijo)
        .addOnSuccessListener {
                    dismiss()
                }
                .addOnFailureListener { e ->
                    if (NetworkUtil.getConnectivityStatus(context!!) !== 0) {
                        dismiss()
                    }
                }
    }


    private fun guardarHijoLocal() {

        mDatabaseHelperHijos!!.updateHijo(viejoID, hijo)
        dismiss()
    }


    private fun showDatePickerDialog() {
        val c = Calendar.getInstance()
        val format = SimpleDateFormat("dd-MM-yyyy", Locale.US)

        if(viejoFecha != ""){
            val dueDate = format.parse(viejoFecha)
            c.time = dueDate
        }
        val year = c.get(Calendar.YEAR)
        val month = c.get(Calendar.MONTH)
        val day = c.get(Calendar.DAY_OF_MONTH)

        val dpd = DatePickerDialog(activity, DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->

            c.set(year, monthOfYear, dayOfMonth)
            anioNacimiento = year
            editFecha!!.text = format.format(c.time)

        }, year, month, day)

        dpd.datePicker.maxDate = c.timeInMillis
        dpd.show()
    }

    private fun validateFields():Boolean{
        val c = Calendar.getInstance()
        val year = c.get(Calendar.YEAR)
        val fecha = editFecha.text.toString().trim()
        val nombre = editNombre.text.toString().trim()
        val peso = editPeso.text.toString().trim()
        val altura = editAltura.text.toString().trim()
        val bebe : Int
        val format = SimpleDateFormat("dd-MM-yyyy", Locale.US)
        val regex = Regex(pattern = "\\w")

        edad = getMonths(fecha).toInt()


        if (TextUtils.isEmpty(nombre) || nombre.isEmpty()) {
            editNombre.error = "Por favor ingresa el nombre de tu hijo"
            return false
        }
        if (nombre.length !in 5..25) {
            editNombre.error = "El nombre debe tener entre 5 y 25 caracteres"
            return false
        }
        if(!regex.containsMatchIn(input = ""+nombre) ){
            editNombre.error = "El nombre no debe tener caracteres especiales"
            return false
        }

        if(fecha == ""){
            editFecha!!.error = "Por favor, ingresa la fecha de nacimiento de tu hijo"
            return false
        }
        if(edad < 0){
            editFecha!!.error = "Por favor, ingresa la fecha de nacimiento de tu hijo"
            return false
        }
        if(edad > 12*18){
            editFecha!!.error = "Nuestra sólo aplicación provee información para niños (0-17 años)"
            return false
        }
        if (TextUtils.isEmpty(peso) || peso.isEmpty()) {
            editAltura.error = "Por favor ingresa la altura de tu hijo"
            return false
        }
        if (Integer.parseInt(peso) > 10000) {
            editPeso.error = "Por favor ingresa un peso válido (en g)"
            return false
        }
        if (TextUtils.isEmpty(altura) || altura.isEmpty()) {
            editAltura.error = "Por favor ingresa la altura de tu hijo"
            return false
        }
        if (Integer.parseInt(altura) > 200) {
            editAltura.error = "Por favor ingresa una altura válida (en cm)"
            return false
        }

        when (edad > 3*12) {
            true -> bebe = 0
            false -> bebe = 1
        }

        hijo = Hijo(viejoID, nombre, genero, Integer.parseInt(peso),  edad, fecha, bebe, Integer.parseInt(altura), 0, vacunasAlDiaList)
        return true
    }

    fun cargarDatosFirebase(){
        vacunas = ArrayList()
        db.collection("tutores").document(correoUsuario)
                .collection("hijos")
                .get()
                .addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        for (document in task.result!!) {
                            val hijo = document.toObject(Hijo::class.java)
                            if(hijo.id == viejoID)
                                vacunas = hijo.vacunas
                        }
                        vacunas.sort()
                        mostrarVacunas()
                    }
                }
    }

    private fun getMonths(dateParam: String): Long {

        //Current date
        val calendar = Calendar.getInstance()
        var actualDate = calendar.time
        val format = SimpleDateFormat("dd-MM-yyyy", Locale.US)
        val stringDate = format.format(actualDate)
        actualDate = format.parse(stringDate)

        //Date param
        val dueDate = format.parse(dateParam)

        val long = actualDate.time - dueDate.time
        return long / (2592000000) //~meses
    }

    private fun mostrarVacunas(){
        vacunasAlDiaList = ArrayList()

        for(vacuna in vacunas){
            if(vacuna.edad <= viejaEdad.toInt()){
                vacunasAlDiaList.add(vacuna)
            }
        }
        vacunasListAdapter = VacunaListAdapter(vacunasAlDiaList, R.layout.item_vacuna)
        vacunasView.adapter = vacunasListAdapter
        vacunasView.visibility = View.VISIBLE
    }

    fun makeDialog(titulo:String, mensaje: String){
        val builder = AlertDialog.Builder(context)
        builder.setTitle(titulo)
        builder.setMessage(mensaje)
        builder.setPositiveButton("Aceptar") { _, _ ->
        }
        val dialog = builder.create()
        dialog.show()
    }
}


