package com.devimpact.bhelp.bhelpkotlin.adapters

import android.support.v4.app.Fragment
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.devimpact.bhelp.bhelpkotlin.R
import com.devimpact.bhelp.bhelpkotlin.models.Vacuna
import java.util.*


class VacunaListAdapter( vacunas: ArrayList<Vacuna>, row_data: Int) : RecyclerView.Adapter<VacunaListAdapter.SingleItemRowHolder>(){

    private var itemsList: ArrayList<Vacuna> = vacunas
    var layout: Int = row_data

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): SingleItemRowHolder {
        val v = LayoutInflater.from(viewGroup.context).inflate(layout, null)

        return SingleItemRowHolder(v)
    }

    override fun onBindViewHolder(holder: SingleItemRowHolder, i: Int) {

        val item = itemsList[i]

        holder.descripcion = item.descripcion
        holder.edad = item.edad
        holder.dosis = item.dosis
        holder.checkVacuna!!.setTag(R.integer.key, i)
        holder.checkVacuna.isChecked = item.aplicada != 0

        var dosis = ""
        if(item.dosis == 0){
            dosis = "Única"
        } else if(item.dosis == -1){
            dosis = "Refuerzo"
        } else {
            dosis = "${item.dosis}"
        }
        holder.checkVacuna!!.text = "${item.descripcion}\nDosis: $dosis  Aplicar a los ${item.edad} meses"
    }

    override fun getItemCount(): Int {
        return itemsList.size
    }

    inner class SingleItemRowHolder(view: View) : RecyclerView.ViewHolder(view) {
        var checkVacuna: CheckBox = view.findViewById(R.id.vacunaCB)
        private var aplicada = 0
        lateinit var descripcion: String
        var edad = 0
        var dosis = 0

        init {
            checkVacuna?.setOnCheckedChangeListener { buttonView, isChecked ->
                aplicada = if(isChecked){
                    1
                } else {
                    0
                }
            }
        }
    }
}

