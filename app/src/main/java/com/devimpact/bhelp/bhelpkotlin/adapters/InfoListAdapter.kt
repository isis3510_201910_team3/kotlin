package com.devimpact.bhelp.bhelpkotlin.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import android.widget.TextView
import com.devimpact.bhelp.bhelpkotlin.R
import com.devimpact.bhelp.bhelpkotlin.models.Info

class InfoListAdapter(private val infoList: MutableList<Info>, private val listener: ItemClickListener) : RecyclerView.Adapter<InfoListAdapter.InfoViewHolder>(), Filterable {
    private var infoSearchList: List<Info>? = null

    inner class InfoViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var tema: TextView = view.findViewById(R.id.temaInfo)
        var info: TextView = view.findViewById(R.id.infoInfo)

        init {
            view.setOnClickListener {
                listener.onItemClicked(infoSearchList!![adapterPosition])
            }
        }
    }

    init {
        this.infoSearchList = infoList
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): InfoViewHolder {
        val itemView = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_informacion, parent, false)
        return InfoViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: InfoViewHolder, position: Int) {
        val info = infoSearchList!![position]
        holder.tema.text = info.tema
        holder.info.text = info.informacion
    }

    override fun getItemCount(): Int {
        return infoSearchList!!.size
    }

    override fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(charSequence: CharSequence): Filter.FilterResults {
                val charString = charSequence.toString()
                if (charString.isEmpty()) {
                    infoSearchList = infoList
                } else {
                    val filteredList = ArrayList<Info>()
                    for (row in infoList) {
                        if (row.tema!!.toLowerCase().contains(charString.toLowerCase()) || row.informacion!!.contains(charSequence)) {
                            filteredList.add(row)
                        }
                    }
                    infoSearchList = filteredList
                }
                val filterResults = Filter.FilterResults()
                filterResults.values = infoSearchList
                return filterResults
            }

            override fun publishResults(charSequence: CharSequence, filterResults: Filter.FilterResults) {
                infoSearchList = filterResults.values as ArrayList<Info>
                notifyDataSetChanged()
            }
        }
    }

    interface ItemClickListener {
        fun onItemClicked(info: Info)
    }
}