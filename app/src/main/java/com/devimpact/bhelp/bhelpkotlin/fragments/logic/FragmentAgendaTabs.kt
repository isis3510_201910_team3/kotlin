package com.devimpact.bhelp.bhelpkotlin.fragments.logic


import android.content.Context
import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentTransaction
import android.support.v4.view.ViewPager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.devimpact.bhelp.bhelpkotlin.adapters.AdapterFragmentTabsAgenda
import com.devimpact.bhelp.bhelpkotlin.dialogs.FullScreenDialogCrearTarea
import com.devimpact.bhelp.bhelpkotlin.R

    class FragmentAgendaTabs : Fragment() {

    private val tabIcons = intArrayOf(R.drawable.ic_alimentos, R.drawable.ic_medicinas, R.drawable.ic_citas)
    private lateinit var tabs: TabLayout
    private lateinit var supportFragmentManager: FragmentManager
    private lateinit var nuevaTarea: android.support.design.widget.FloatingActionButton
    private lateinit var viewPager: ViewPager
    lateinit var correoUsuario: String
    private var actualTab = 1

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_agenda, container, false)
        val sharedPref = activity!!.getSharedPreferences(getString(R.string.preference_file_key), Context.MODE_PRIVATE)
        correoUsuario = sharedPref.getString("correoUsuario","")

        supportFragmentManager = activity!!.supportFragmentManager

        val fragmentAdapter = AdapterFragmentTabsAgenda(supportFragmentManager, correoUsuario)
        viewPager = view.findViewById(R.id.viewpager_main)
        tabs = view.findViewById(R.id.tabs_main)
        nuevaTarea = view.findViewById(R.id.nuevaTarea)

        viewPager.adapter = fragmentAdapter
        tabs.setupWithViewPager(viewPager)
        tabs.addOnTabSelectedListener(object :
                TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
                actualTab = tab.position
                val sharedPref = activity!!.getSharedPreferences(getString(R.string.preference_file_key), Context.MODE_PRIVATE)
                sharedPref.edit().putInt("actualTab", actualTab).commit()
            }

            override fun onTabUnselected(tab: TabLayout.Tab) {

            }

            override fun onTabReselected(tab: TabLayout.Tab) {

            }

        })
        setupTabIcons()

        nuevaTarea.setOnClickListener {
            crearFullScreenDialog(actualTab)
        }

        return view
    }

    private fun setupTabIcons() {
        tabs.getTabAt(0)!!.setIcon(tabIcons[0])
        tabs.getTabAt(1)!!.setIcon(tabIcons[1])
        tabs.getTabAt(2)!!.setIcon(tabIcons[2])
    }


    private fun crearFullScreenDialog(actualTab: Int) {
        val fragmentManager = supportFragmentManager
        val newFragment = FullScreenDialogCrearTarea()
        val sharedPref = activity!!.getSharedPreferences(getString(R.string.preference_file_key), Context.MODE_PRIVATE)
        sharedPref.edit().putInt("actualTab", actualTab).commit()

        val transaction = fragmentManager.beginTransaction()
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
        transaction.add(android.R.id.content, newFragment, "FullScreenFragment")
                .addToBackStack(null) //agregado para poder ir atrás
                .commit()
    }
}



