package com.devimpact.bhelp.bhelpkotlin.dialogs

import android.app.*
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.text.TextUtils
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.*
import com.devimpact.bhelp.bhelpkotlin.R
import com.devimpact.bhelp.bhelpkotlin.databasehelpers.DatabaseHelper
import com.devimpact.bhelp.bhelpkotlin.fragments.agenda.AgendaFragmentInterface
import com.devimpact.bhelp.bhelpkotlin.fragments.agenda.FragmentAlimentos
import com.devimpact.bhelp.bhelpkotlin.fragments.agenda.FragmentCitas
import com.devimpact.bhelp.bhelpkotlin.fragments.agenda.FragmentMedicinas
import com.devimpact.bhelp.bhelpkotlin.models.Hijo
import com.devimpact.bhelp.bhelpkotlin.models.TareaAdapter
import com.devimpact.bhelp.bhelpkotlin.models.Vacuna
import com.devimpact.bhelp.bhelpkotlin.services.NetworkUtil
import com.devimpact.bhelp.bhelpkotlin.services.NotificationBroadcastReceiver
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.dialog_crear_tarea.*
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList


class FullScreenDialogCrearTarea : FullScreenDialogInterface() {

    private val db by lazy { FirebaseFirestore.getInstance() }
    private var instanceTimeCreacion: Calendar? = null
    private var textFecha: TextView? = null
    private var textTiempo: TextView? = null
    private lateinit var editCategoria: Spinner
    private lateinit var editPrioridad: Spinner
    private lateinit var editTitulo: EditText
    private lateinit var editDescripcion: EditText
    private lateinit var editFecha: TextView
    private lateinit var editHora: TextView
    private lateinit var buttonGuardar: TextView
    private lateinit var tarea: TareaAdapter
    var mDatabaseHelper: DatabaseHelper? = null
    private lateinit var supportFragmentManager: FragmentManager
    private lateinit var fragmentTareas: Fragment
    lateinit var correoUsuario: String
    private lateinit var hijosList: ArrayList<Hijo>
    private lateinit var hijosListName: ArrayList<String>
    private var idAlarma = 0
    private var idG = ""
    private var idHijo = ""


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.dialog_crear_tarea, container, false)
        iniciarHora(view)// Setear hora inicial
        iniciarFecha(view)// Setear fecha inicial

        val sharedPref = activity!!.getSharedPreferences(getString(R.string.preference_file_key), Context.MODE_PRIVATE)
        correoUsuario = sharedPref.getString("correoUsuario", "")
        mDatabaseHelper = DatabaseHelper(activity!!)

        supportFragmentManager = activity!!.supportFragmentManager

        val close = view.findViewById<ImageButton>(R.id.ic_close)
        buttonGuardar = view.findViewById(R.id.guardarTarea)
        editTitulo = view.findViewById(R.id.tituloTarea)
        editDescripcion = view.findViewById(R.id.descripcionTarea)
        editFecha = view.findViewById(R.id.fecha_text)
        editHora = view.findViewById(R.id.hora_text)
        editCategoria = view.findViewById(R.id.categoria)
        editPrioridad = view.findViewById(R.id.prioridad)

        hijosList = ArrayList()
        hijosListName = ArrayList()
        val ref = db.collection("tutores").document(correoUsuario)
                .collection("hijos")
        var hijo: Hijo
        var adapter: ArrayAdapter<String>

        ref.get()
                .addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        for (document in task.result!!) {
                            hijo = document.toObject(Hijo::class.java)
                            hijosList.add(hijo)
                            hijosListName.add(hijo.nombre)
                        }
                        adapter = ArrayAdapter(activity,
                                android.R.layout.simple_spinner_item, hijosListName)
                        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                        spinner_hijo.adapter = adapter
                    }
                }



        for (i in supportFragmentManager.fragments.iterator()) {
            if (i.toString().contains("FragmentAlimentos") || i.toString().contains("FragmentMedicinas") || i.toString().contains("FragmentCitas")) {
                fragmentTareas = i
            }
        }

        val tab = sharedPref.getInt("actualTab", 0)
        editCategoria.setSelection(tab)

        close.setOnClickListener {
            dismiss()
        }
        buttonGuardar.setOnClickListener {
            if (validateFields()) {
                if (NetworkUtil.getConnectivityStatus(activity!!) == 0) {
                    makeDialog("Error de conexión", "En este momento no estás conectado a internet, la información se guardará localmente y cuando vuelvas a conectarte la sincronizaremos")
                }

                guardarTareaLocal()
                guardarTareaFirebase()
                createAlarm()
            }
            val categoria = editCategoria.selectedItem.toString()
            val fragment: AgendaFragmentInterface
            Log.d("asd", categoria)
            if (categoria == "Alimentos") {
                fragment = supportFragmentManager.findFragmentByTag("android:switcher:${fragmentTareas.id}:0") as FragmentAlimentos
            } else if (categoria == "Medicinas") {
                fragment = supportFragmentManager.findFragmentByTag("android:switcher:${fragmentTareas.id}:1") as FragmentMedicinas
            } else {
                fragment = supportFragmentManager.findFragmentByTag("android:switcher:${fragmentTareas.id}:2") as FragmentCitas
            }

            fragment.actualizar(true)


            val imm = activity!!.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(activity!!.currentFocus.windowToken, 0)
        }
        return view
    }

    private fun createAlarm() {

        val current = System.currentTimeMillis()
        val alarmMgr = activity!!.getSystemService(Context.ALARM_SERVICE) as AlarmManager

        val broadcastIntent = Intent(activity, NotificationBroadcastReceiver::class.java)

        val fechaHoraTarea = tarea.fecha
        val long = getTimeUntilDate(fechaHoraTarea)

        broadcastIntent.putExtra("title", tarea.titulo)
        broadcastIntent.putExtra("id", "$idAlarma")

        val pIntent = PendingIntent.getBroadcast(activity, idAlarma, broadcastIntent, 0)


        Log.d("ALARMA", "${tarea.titulo}:${long / 1000}s")
        if (long > 0) {
            alarmMgr.set(AlarmManager.RTC_WAKEUP, current + long, pIntent)
        }
    }

    private fun getTimeUntilDate(dateParam: String): Long {

        //Current date
        val calendar = Calendar.getInstance()
        var actualDate = calendar.time
        val format = SimpleDateFormat("dd-MM-yyyy HH:mm a", Locale.US)
        val stringDate = format.format(actualDate)
        actualDate = format.parse(stringDate)

        //Date param
        val dueDate = format.parse(dateParam)

        calendar.time = dueDate
        idAlarma = ("" + calendar.get(Calendar.DAY_OF_MONTH) + calendar.get(Calendar.HOUR_OF_DAY) + calendar.get(Calendar.MINUTE) + calendar.get(Calendar.SECOND)).toInt()

        return dueDate.time - actualDate.time
    }

    private fun iniciarHora(view: View) {
        textTiempo = view.findViewById(R.id.hora_text) as TextView
        val c = Calendar.getInstance()
        val format = SimpleDateFormat("HH:mm a", Locale.US)
        textTiempo!!.text = format.format(c.time)

        //textTiempo!!.setOnClickListener { TimeDialog().show(fragmentManager, "TimePickerInFull") }
        textTiempo!!.setOnClickListener { showTimePickerDialog() }
    }

    private fun iniciarFecha(view: View) {
        textFecha = view.findViewById(R.id.fecha_text) as TextView
        val c = Calendar.getInstance()
        val format = SimpleDateFormat("dd-MM-yyyy", Locale.US)
        textFecha!!.text = format.format(c.time)

        //textFecha!!.setOnClickListener { DateDialog().show(fragmentManager, "DatePickerInFull") }
        textFecha!!.setOnClickListener { showDatePickerDialog() }
    }

    private fun showDatePickerDialog() {
        val c = Calendar.getInstance()
        val year = c.get(Calendar.YEAR)
        val month = c.get(Calendar.MONTH)
        val day = c.get(Calendar.DAY_OF_MONTH)

        val dpd = DatePickerDialog(activity, DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->

            c.set(year, monthOfYear, dayOfMonth)
            val format = SimpleDateFormat("dd-MM-yyyy", Locale.US)
            textFecha!!.text = format.format(c.time)

        }, year, month, day)

        dpd.datePicker.minDate = c.timeInMillis
        dpd.show()
    }

    private fun showTimePickerDialog() {
        val c = Calendar.getInstance()
        instanceTimeCreacion = c
        val hour = c.get(Calendar.HOUR_OF_DAY)
        val minute = c.get(Calendar.MINUTE)

        val tpd = TimePickerDialog(activity, TimePickerDialog.OnTimeSetListener(function = { view, h, m ->

            c.set(Calendar.HOUR_OF_DAY, h)
            c.set(Calendar.MINUTE, m)

            val format = SimpleDateFormat("HH:mm a", Locale.US)
            format.timeZone = (TimeZone.getTimeZone("GMT-05:00"))
            textTiempo!!.text = format.format(c.time)

        }), hour, minute, false)

        tpd.show()

    }


    private fun guardarTareaFirebase() {
        val categoria = editCategoria.selectedItem.toString()
        val coleccionPorCategoria = "tareas$categoria"

        val rootRef = FirebaseFirestore.getInstance()
        var ref = rootRef
                .collection("tutores").document(correoUsuario)
                .collection(coleccionPorCategoria)
        ref.document(tarea.id).set(tarea)
                .addOnFailureListener { e ->
                    if (NetworkUtil.getConnectivityStatus(context!!) !== 0) {
                        noGuarda()
                    }
                }

        ref = rootRef
                .collection("tutores").document(correoUsuario)
                .collection("hijos").document(idHijo)
                .collection("tareas")
        ref.document(tarea.id).set(tarea)
                .addOnFailureListener { e ->
                    if (NetworkUtil.getConnectivityStatus(context!!) !== 0) {
                        noGuarda()
                    }
                }

        if(tarea.prioridad == "Alta"){
            ref = rootRef
                    .collection("tutores").document(correoUsuario)
                    .collection("hijos").document(idHijo)
                    .collection("tareasPrioridadAlta")
            ref.document(tarea.id).set(tarea)
                    .addOnFailureListener { e ->
                        if (NetworkUtil.getConnectivityStatus(context!!) !== 0) {
                            noGuarda()
                        }
                    }
        }
    }

    private fun guardarTareaLocal() {
        val partesFecha = tarea.fecha.split(" ")

        if (super.guardarTareaLocal(tarea.titulo, partesFecha[0], partesFecha[1], editCategoria.selectedItem.toString(), tarea.prioridad, tarea.descripcion,
                        0, mDatabaseHelper!!)) {
            dismiss()
        } else {
            noGuarda()
        }
    }

    private fun noGuarda() {
        makeDialog("Error creando tarea", "No se pudo crear la tarea, por favor vuelve a intentarlo")
        dismiss()
    }


    private fun validateFields(): Boolean {

        val titulo = editTitulo.text.toString().trim()
        val descripcion = editDescripcion.text.toString().trim()
        val fecha = editFecha.text.toString().trim()
        val hora = editHora.text.toString().trim()
        val regex = Regex(pattern = "a|b|c|d|e|f|g|h|i|j|k|l|m|n|ñ|o|p|q|r|s|t|v|w|x|y|z|A|B|C|D|E|F|G|H|I|J|K|L|M|N|Ñ|O|P|Q|R|S|T|U|V|W|X|Y|Z")


        if (TextUtils.isEmpty(titulo) || titulo.isEmpty()) {
            editTitulo.error = "Por favor ingresa un título para la nueva tarea"
            return false
        }

        if (titulo.length !in 5..25) {
            editTitulo.error = "El título debe tener entre 5 y 25 caracteres"
            return false
        }
        if (!regex.containsMatchIn(input = "" + titulo)) {
            editTitulo.error = "El título no debe tener caracteres especiales"
            return false
        }
        if (descripcion.length > 35) {
            editDescripcion.error = "La descipción no puede tener más de 35 caracteres"
            return false
        }


        idG = "$titulo-$fecha $hora"

        for(hijo in hijosList){
            if (hijo.nombre == spinner_hijo.selectedItem){
                idHijo = hijo.id
            }
        }

        tarea = TareaAdapter(idG, titulo, "$fecha $hora", editPrioridad.selectedItem.toString(), categoria.selectedItem.toString(), 0, editDescripcion.text.toString(), idHijo)

        return true
    }

    fun makeDialog(titulo:String, mensaje: String){
        val builder = AlertDialog.Builder(context)
        builder.setTitle(titulo)
        builder.setMessage(mensaje)
        builder.setPositiveButton("Aceptar") { _, _ ->
        }
        val dialog = builder.create()
        dialog.show()
    }

}

