package com.devimpact.bhelp.bhelpkotlin.databasehelpers

import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import com.devimpact.bhelp.bhelpkotlin.models.Tarea
import com.devimpact.bhelp.bhelpkotlin.models.TareaAdapter

class DatabaseHelper(context: Context) : SQLiteOpenHelper(context, TABLE_NAME, null, 1) {

    /**
     * Returns all the data from database
     * @return
     */
    val data: Cursor
        get() {
            val db = this.writableDatabase
            val query = "SELECT * FROM $TABLE_NAME"
            return db.rawQuery(query, null)
        }

    fun getDatos(): Cursor {
        val db = this.writableDatabase
        val query = "SELECT * FROM $TABLE_NAME"
        return db.rawQuery(query, null)
    }

    override fun onCreate(db: SQLiteDatabase) {
        val createTable = "CREATE TABLE " + TABLE_NAME + " (ID TEXT PRIMARY KEY, " +
                COL2 + " TEXT," + COL3 + " TEXT," + COL4 + " TEXT," + COL5 + " TEXT," + COL6 + " TEXT," + COL8 + " INTEGER," + COL7 + " TEXT)"
        db.execSQL(createTable)
    }

    override fun onUpgrade(db: SQLiteDatabase, i: Int, i1: Int) {
        db.execSQL("DROP TABLE IF EXISTS $TABLE_NAME")
        onCreate(db)
    }

    fun getTareasXCategoria(categoria: String): Cursor {
        val db = this.writableDatabase
        val query = "SELECT * FROM " + TABLE_NAME +
                " WHERE " + COL7 + " = '" + categoria + "'"
        return db.rawQuery(query, null)
    }

    fun getTareasXPrioridad(prioridad: String): Cursor {
        val db = this.writableDatabase
        val query = "SELECT * FROM " + TABLE_NAME +
                " WHERE " + COL4 + " = '" + prioridad + "'"
        return db.rawQuery(query, null)
    }

    fun addData(id:String, tarea: TareaAdapter, categoria: String): Boolean {
        val db = this.writableDatabase
        val contentValues = ContentValues()
        contentValues.put(COL1, id)
        contentValues.put(COL2, tarea.titulo)
        contentValues.put(COL3, tarea.descripcion)
        contentValues.put(COL4, tarea.prioridad)
        contentValues.put(COL5, tarea.fecha.split(" ")[0])
        contentValues.put(COL6, tarea.fecha.split(" ")[1])
        contentValues.put(COL8, tarea.compartida)
        contentValues.put(COL7, categoria)

        val result = db.insert(TABLE_NAME, null, contentValues)

        //if date as inserted incorrectly it will return -1
        return (result !== -1L)
    }

    /**
     * Updates the name field
     * @param newName
     * @param id
     * @param oldName
     */
    fun updateTarea(id:String, nuevaTarea: Tarea, titulo: String, categoria: String) {
        val db = this.writableDatabase
        val query = "UPDATE " + TABLE_NAME + " SET " + COL2 +
                " = '" + nuevaTarea.titulo + "', " + COL3 +
                " = '" + nuevaTarea.descripcion + "', " + COL4 +
                " = '" + nuevaTarea.prioridad + "', " + COL5 +
                " = '" + nuevaTarea.fecha + "', " + COL6 +
                " = '" + nuevaTarea.hora + "', " + COL7 +
                " = '" + nuevaTarea.compartido + "', " + COL8 +
                " = '" + categoria + "' WHERE " + COL2 + " = '" + titulo+"'"
        db.execSQL(query)
    }

    /**
     * Delete from database
     * @param id
     * @param name
     */
    fun deleteName(titulo: String) {
        val db = this.writableDatabase
        val query = ("DELETE FROM " + TABLE_NAME + " WHERE "
                + COL2 + " = '" + titulo + "'")
        db.execSQL(query)
    }

    companion object {

        private val TABLE_NAME = "tareas_table"
        private val COL1 = "ID"
        private val COL2 = "titulo"
        private val COL3 = "descripcion"
        private val COL4 = "prioridad"
        private val COL5 = "fecha"
        private val COL6 = "hora"
        private val COL7 = "categoria"
        private val COL8 = "compartido"
    }
}

