package com.devimpact.bhelp.bhelpkotlin.adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.GestureDetector
import android.view.MotionEvent
import android.view.View

class RecyclerTouchListener(context: Context, recyclerView: RecyclerView, private var clickListener: ClickListener?) : RecyclerView.OnItemTouchListener {

    private var gestureDetector: GestureDetector = GestureDetector(context, object : GestureDetector.SimpleOnGestureListener() {
        override fun onSingleTapUp(e: MotionEvent): Boolean {

            return true

        }
    })

    override fun onInterceptTouchEvent(rv: RecyclerView, e: MotionEvent): Boolean {

        val child = rv.findChildViewUnder(e.x, e.y)

        if (child != null && clickListener != null && gestureDetector.onTouchEvent(e)) {

            clickListener!!.onClick(child, rv.getChildAdapterPosition(child))

        }
        return false
    }

    override fun onTouchEvent(rv: RecyclerView, e: MotionEvent) {}

    override fun onRequestDisallowInterceptTouchEvent(disallowIntercept: Boolean) {

    }

    interface ClickListener {
        fun onClick(view: View, position: Int)
    }
}