package com.devimpact.bhelp.bhelpkotlin.services

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import android.media.AudioAttributes
import android.media.MediaPlayer
import android.net.Uri
import android.os.Build
import android.support.v4.app.NotificationCompat
import com.devimpact.bhelp.bhelpkotlin.activity.Menu
import com.devimpact.bhelp.bhelpkotlin.R

class NotificationBroadcastReceiver : BroadcastReceiver() {

    override fun onReceive(context: Context?, intent: Intent?) {



        val title = intent!!.getStringExtra("title")
        val id = intent!!.getIntExtra("id", 0)

        val intent1 = Intent(context, Menu::class.java)
        intent1.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        val pendingIntent = PendingIntent.getActivity(context, 0 /* Request code */, intent1,
                PendingIntent.FLAG_ONE_SHOT)

        val bitmap = BitmapFactory.decodeResource(context!!.resources, R.drawable.ic_baby)

        val vibration = longArrayOf(200, 2000)

        val channelId = "default_channel_id"

        val notificationManager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        // Since android Oreo notification channel is needed.
        val uri = Uri.parse("android.resource://${context.packageName}/${R.raw.notification}")

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            var attributes = AudioAttributes.Builder()
                    .setUsage(AudioAttributes.USAGE_NOTIFICATION)
                    .build()

            val channel = NotificationChannel(channelId,
                    "Channel human readable title",
                    NotificationManager.IMPORTANCE_HIGH)
            notificationManager.createNotificationChannel(channel)
        }

        val style = NotificationCompat.BigTextStyle()
        style.setBigContentTitle("BHelp")
        style.bigText(title)

        val notificationBuilder = NotificationCompat.Builder(context, channelId)
                .setLargeIcon(bitmap)
                .setSmallIcon(R.drawable.ic_baby)
                .setContentTitle("BHelp")
                .setContentText(title)
                .setAutoCancel(true)
                .setVibrate(vibration)
                .setContentIntent(pendingIntent)
                .setSound(uri)
                .setStyle(style)
                .setColor(context.resources.getColor(R.color.colorPrimary))



        notificationManager.notify(id /* ID of notification */, notificationBuilder.build())

        val mp = MediaPlayer.create(context, uri)
        mp.start()

    }
}
