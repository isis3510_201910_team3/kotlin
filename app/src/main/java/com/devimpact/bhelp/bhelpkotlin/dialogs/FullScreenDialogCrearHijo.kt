package com.devimpact.bhelp.bhelpkotlin.dialogs

import android.app.AlarmManager
import android.app.AlertDialog
import android.app.DatePickerDialog
import android.app.PendingIntent
import android.content.Context
import android.content.Context.INPUT_METHOD_SERVICE
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.text.TextUtils
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.*
import android.widget.EditText
import com.devimpact.bhelp.bhelpkotlin.adapters.RecyclerTouchListener
import com.devimpact.bhelp.bhelpkotlin.adapters.VacunaListAdapter
import com.devimpact.bhelp.bhelpkotlin.databasehelpers.DatabaseHelperHijos
import com.devimpact.bhelp.bhelpkotlin.fragments.logic.FragmentHijos
import com.devimpact.bhelp.bhelpkotlin.fragments.logic.FragmentProfileHome
import com.devimpact.bhelp.bhelpkotlin.services.NetworkUtil
import com.devimpact.bhelp.bhelpkotlin.R
import com.devimpact.bhelp.bhelpkotlin.services.NotificationBroadcastReceiver
import com.devimpact.bhelp.bhelpkotlin.models.Hijo
import com.devimpact.bhelp.bhelpkotlin.models.Vacuna
import com.google.firebase.firestore.FirebaseFirestore
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList




class FullScreenDialogCrearHijo : DialogFragment() {

    private lateinit var editNombre: EditText
    private lateinit var editPeso: EditText
    private lateinit var editAltura: EditText
    private var textFecha: TextView? = null
    private lateinit var editFecha: TextView
    private lateinit var rdbGeneroM: RadioButton
    private lateinit var rdbGeneroF: RadioButton

    private var genero = 1
    private var edad = 0
    private var yearG = -1
    private var anioNacimiento = -1

    private lateinit var buttonGuardar: TextView
    private var mDatabaseHelperHijos: DatabaseHelperHijos? = null
    private lateinit var supportFragmentManager: FragmentManager
    private lateinit var fragmentHijos: Fragment
    private lateinit var hijo: Hijo
    private lateinit var fragmentHome: FragmentProfileHome

    private lateinit var vacunasView: RecyclerView
    private lateinit var vacunasListAdapter: VacunaListAdapter
    private lateinit var vacunasAlDiaList: ArrayList<Vacuna>
    private lateinit var vacunas: ArrayList<Vacuna>
    lateinit var correoUsuario: String
    private var idG = ""

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.dialog_crear_hijo, container, false)
        mDatabaseHelperHijos = DatabaseHelperHijos(activity!!)
        supportFragmentManager = activity!!.supportFragmentManager
        val sharedPref = activity!!.getSharedPreferences(getString(R.string.preference_file_key), Context.MODE_PRIVATE)
        correoUsuario = sharedPref.getString("correoUsuario","")

        val close = view.findViewById<ImageButton>(R.id.ic_close)
        buttonGuardar = view.findViewById(R.id.guardarHijo)
        editNombre = view.findViewById(R.id.nombreHijo)
        editPeso = view.findViewById(R.id.pesoHijo)
        editAltura = view.findViewById(R.id.alturaHijo)
        textFecha = view.findViewById(R.id.fechaNac_text)
        editFecha = view.findViewById(R.id.fechaNac_text)
        rdbGeneroF = view.findViewById(R.id.femenino)
        rdbGeneroM = view.findViewById(R.id.masculino)

        vacunasAlDiaList = ArrayList()
        vacunasView = view.findViewById(R.id.vacunas)
        vacunasView.layoutManager = GridLayoutManager(this.activity, 1)


        vacunasView.addOnItemTouchListener(RecyclerTouchListener(
                context!! ,
                vacunasView ,
                object : RecyclerTouchListener.ClickListener
                {
                    override fun onClick(view: View, position: Int) {
                        vacunas[position].aplicada = 1 - vacunas[position].aplicada
                    }
                }))

        anioNacimiento = -1

        rdbGeneroF.setOnClickListener {
            genero = 1
        }
        rdbGeneroM.setOnClickListener {
            genero = 0
        }


        for(i in supportFragmentManager.fragments.iterator())
        {
            if(i.toString().contains("FragmentHijos") ) {
                fragmentHijos = i
            }
        }

        cargarDatosVacunas()

        textFecha!!.setOnClickListener { showDatePickerDialog() }
        close.setOnClickListener {
            dismiss()
        }
        buttonGuardar.setOnClickListener {
            if(validateFields()) {
                if (NetworkUtil.getConnectivityStatus(activity!!) == 0) {
                    makeDialog("Error de conexión", "En este momento no estás conectado a internet, la información se guardará localmente y cuando vuelvas a conectarte la sincronizaremos")
                }
                guardarHijoLocal()
                guardarHijoFirebase()
                recordatoriosVacunas()
                val imm = activity!!.getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
                imm.hideSoftInputFromWindow(activity!!.currentFocus.windowToken,0)
            }
            val fragment = supportFragmentManager.findFragmentByTag("fragmentHijos") as FragmentHijos
            fragment.actualizar(true)

            val shared = activity!!.getPreferences(Context.MODE_PRIVATE)
            val profileHome = shared.getBoolean("desdeProfileHome", false)
            if(profileHome){
                fragmentHome = FragmentProfileHome()

                val args = Bundle()
                args.putString("nombre", editNombre.text.toString())
                args.putString("peso", editPeso.text.toString())
                args.putString("altura", editAltura.text.toString())

                fragmentHome.arguments = args
                //arreglar esto: que no se dibuje encima del anterior que se está creando en FragmentsTabsHome
                supportFragmentManager.beginTransaction().remove(fragmentHome).commit()
                supportFragmentManager.beginTransaction().add(R.id.containerHome, fragmentHome).commit()
            }
        }

        return view
    }


    private fun guardarHijoFirebase() {

        val rootRef = FirebaseFirestore.getInstance()
        val ref = rootRef
                .collection("tutores").document(correoUsuario)
                .collection("hijos")

        ref.document(hijo.id).set(hijo)
                .addOnSuccessListener {
                    dismiss()
                }
                .addOnFailureListener { e ->
                    if (NetworkUtil.getConnectivityStatus(context!!) !== 0) {
                        dismiss()
                    }
                }
    }


    private fun guardarHijoLocal() {

        if(mDatabaseHelperHijos!!.addData(idG, hijo))
        {
            dismiss()
        }else {
            dismiss()
        }

        if(mDatabaseHelperHijos!!.getDatos().count == 1)
        {
            fragmentHome = FragmentProfileHome()

            val args = Bundle()
            args.putString("nombre", hijo.nombre)
            args.putString("peso", hijo.peso.toString())
            args.putString("altura", hijo.altura.toString())
            args.putInt("bebe", hijo.bebe)
            args.putInt("sexo", hijo.sexo)

            fragmentHome.arguments = args
            //arreglar esto: que no se dibuje encima del anterior que se está creando en FragmentsTabsHome
            supportFragmentManager.beginTransaction().remove(fragmentHome).commit()
            supportFragmentManager.beginTransaction().add(R.id.containerHome, fragmentHome).commit()
        }
    }

    private fun showDatePickerDialog() {
        val c = Calendar.getInstance()
        val year = c.get(Calendar.YEAR)
        val month = c.get(Calendar.MONTH)
        val day = c.get(Calendar.DAY_OF_MONTH)
        yearG = year

        val dpd = DatePickerDialog(activity, DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
            c.set(year, monthOfYear, dayOfMonth)
            val format = SimpleDateFormat("dd-MM-yyyy", Locale.US)
            val fecha = format.format(c.time)
            textFecha!!.text = fecha

            edad = getMonths(fecha).toInt()

            mostrarVacunas()
        }, year, month, day)

        dpd.datePicker.maxDate = c.timeInMillis
        dpd.show()
    }

    private fun validateFields():Boolean{
        val fecha = editFecha.text.toString().trim()
        val nombre = editNombre.text.toString().trim()
        val peso = editPeso.text.toString().trim()
        val altura = editAltura.text.toString().trim()
        val format = SimpleDateFormat("dd-MM-yyyy", Locale.US)
        val bebe : Int

        val c = Calendar.getInstance()
        val year = c.get(Calendar.YEAR)


        val regex = Regex(pattern = "a|b|c|d|e|f|g|h|i|j|k|l|m|n|ñ|o|p|q|r|s|t|v|w|x|y|z|A|B|C|D|E|F|G|H|I|J|K|L|M|N|Ñ|O|P|Q|R|S|T|U|V|W|X|Y|Z")

        if (TextUtils.isEmpty(nombre) || nombre.isEmpty()) {
            editNombre.error = "Por favor ingresa el nombre de tu hijo"
            return false
        }
        if (nombre.length !in 5..25) {
            editNombre.error = "El nombre debe tener entre 5 y 25 caracteres"
            return false
        }
        if(!regex.containsMatchIn(input = ""+nombre) ){
            editNombre.error = "El nombre no debe tener caracteres especiales"
            return false
        }

        if(fecha == ""){
            textFecha!!.error = "Por favor, ingresa la fecha de nacimiento de tu hijo"
            return false
        }
        if(edad < 0){
            textFecha!!.error = "Por favor, ingresa la fecha de nacimiento de tu hijo"
            return false
        }
        if(edad > 12*18){
            textFecha!!.error = "Nuestra sólo aplicación provee información para niños (0-17 años)"
            return false
        }
        if (TextUtils.isEmpty(peso) || peso.isEmpty()) {
            editAltura.error = "Por favor ingresa la altura de tu hijo"
            return false
        }
        if (Integer.parseInt(peso) > 10000) {
            editPeso.error = "Por favor ingresa un peso válido (en g)"
            return false
        }
        if (TextUtils.isEmpty(altura) || altura.isEmpty()) {
            editAltura.error = "Por favor ingresa la altura de tu hijo"
            return false
        }
        if (Integer.parseInt(altura) > 200) {
            editAltura.error = "Por favor ingresa una altura válida (en cm)"
            return false
        }

        idG = "$nombre-$fecha"

        bebe = when (edad > 3*12) {
            true -> 0
            false -> 1
        }

        hijo = Hijo(idG, nombre, genero, Integer.parseInt(peso),  edad, fecha, bebe,  Integer.parseInt(altura), 0, vacunasAlDiaList)
        return true
    }

    private fun cargarDatosVacunas(){
        vacunas = ArrayList()
        vacunas.add(Vacuna("Hepatitis B",0,1,0))
        vacunas.add(Vacuna("Neumococo",2,1,0))
        vacunas.add(Vacuna("Neumococo",4,2,0))
        vacunas.add(Vacuna("Influenza",6,1,0))
        vacunas.add(Vacuna("Influenza",7,2,0))
        vacunas.add(Vacuna("Fiebre amarilla",12,1,0))
        vacunas.add(Vacuna("Difteria-Tosferina-Tétano",18,1,0))
        vacunas.add(Vacuna("Difteria-Tosferina-Tétano",60,2,0))

        vacunasListAdapter = VacunaListAdapter( vacunas, R.layout.item_vacuna)
        vacunasView.adapter = vacunasListAdapter
        vacunasView.visibility = View.VISIBLE
    }

    private fun mostrarVacunas(){
        vacunasAlDiaList = ArrayList()

        for(vacuna in vacunas){
            if(vacuna.edad <= edad){
                vacunasAlDiaList.add(vacuna)
            }
        }
        vacunasListAdapter = VacunaListAdapter( vacunasAlDiaList, R.layout.item_vacuna)
        vacunasView.adapter = vacunasListAdapter
        vacunasView.visibility = View.VISIBLE
    }

    private fun getMonths(dateParam: String): Long {
        //Current date
        val calendar = Calendar.getInstance()
        var actualDate = calendar.time
        val format = SimpleDateFormat("dd-MM-yyyy", Locale.US)
        val stringDate = format.format(actualDate)
        actualDate = format.parse(stringDate)

        //Date param
        val dueDate = format.parse(dateParam)

        val long = actualDate.time - dueDate.time
        return long / (2592000000) //~meses
    }

    private fun dateForXMonths(birthDate: Date, months: Int): Date{
        val calendar = Calendar.getInstance()
        calendar.time = birthDate
        calendar.add(Calendar.MONTH, months)
        return calendar.time
    }

    private fun recordatoriosVacunas(){
        val calendar = Calendar.getInstance()
        val calendar2 = Calendar.getInstance()
        val format = SimpleDateFormat("dd-MM-yyyy", Locale.US)
        //Fecha nacimiento
        calendar.time = format.parse(editFecha.text as String?)
        val alarmMgr = activity!!.getSystemService(Context.ALARM_SERVICE) as AlarmManager

        var broadcastIntent: Intent
        var fechaAplicarVacuna:Date
        var pIntent: PendingIntent
        var idAlarma = 0
        var delay = 30000
        val horaDia = 28800000
        for(vacuna in vacunas){
            if(vacuna.aplicada == 0) {
                broadcastIntent = Intent(activity, NotificationBroadcastReceiver::class.java)
                fechaAplicarVacuna = dateForXMonths(calendar.time, vacuna.edad)
                calendar2.time = fechaAplicarVacuna
                idAlarma = ("" + calendar2.get(Calendar.YEAR) + calendar2.get(Calendar.MONTH) + calendar2.get(Calendar.HOUR_OF_DAY) + calendar2.get(Calendar.MINUTE) + calendar2.get(Calendar.SECOND)).toInt()
                broadcastIntent.putExtra("title", "Felicitaciones! Hoy ${hijo.nombre} cumple ${vacuna.edad} meses. Recuerda que es momento de aplicar la vacuna contra ${vacuna.descripcion}")
                broadcastIntent.putExtra("id", idAlarma)
                pIntent = PendingIntent.getBroadcast(activity, idAlarma, broadcastIntent, 0)
                fechaAplicarVacuna.time += horaDia
                if(fechaAplicarVacuna.time > System.currentTimeMillis()) {
                    alarmMgr.set(AlarmManager.RTC_WAKEUP, fechaAplicarVacuna.time, pIntent)
                }
            }
        }

    }

    fun makeDialog(titulo:String, mensaje: String){
        val builder = AlertDialog.Builder(context)
        builder.setTitle(titulo)
        builder.setMessage(mensaje)
        builder.setPositiveButton("Aceptar") { _, _ ->
        }
        val dialog = builder.create()
        dialog.show()
    }

}


