package com.devimpact.bhelp.bhelpkotlin.threads

import android.util.ArrayMap
import com.devimpact.bhelp.bhelpkotlin.databasehelpers.DatabaseHelper
import com.devimpact.bhelp.bhelpkotlin.activity.Menu
import com.devimpact.bhelp.bhelpkotlin.services.NetworkUtil
import com.devimpact.bhelp.bhelpkotlin.models.TareaAdapter
import com.google.firebase.firestore.FirebaseFirestore
import java.lang.ref.WeakReference

class SincronizarTareasAsyncTask(context: Menu, correo: String) : Thread() {

    private val db by lazy { FirebaseFirestore.getInstance() }
    private var resp: String? = null
    private var correoUsuario = correo
    private val activityReference: WeakReference<Menu> = WeakReference(context)
    private var mDatabaseHelper: DatabaseHelper? = null
    private lateinit var tareasSQLite: ArrayMap<String, TareaAdapter>
    private lateinit var tareasFirebase: ArrayMap<String, TareaAdapter>

    override fun run() {
        super.run()
        try {
            val activity = activityReference.get()
            mDatabaseHelper = DatabaseHelper(activity!!.applicationContext)
            cargarDatosSQLite()

            if (NetworkUtil.getConnectivityStatus(activity!!.applicationContext) != 0) {
                cargarDatosFirebase()
            }

        } catch (e: InterruptedException) {
            e.printStackTrace()
            resp = e.message
        } catch (e: Exception) {
            e.printStackTrace()
            resp = e.message
        }

    }

    private fun cargarDatosSQLite() {
        tareasSQLite = ArrayMap()
        val data = mDatabaseHelper!!.getDatos()
        while (data!!.moveToNext()) {
            val tarea = TareaAdapter(data.getString(0), data.getString(1), "${data.getString(4)};${data.getString(5)}", data.getString(3), data.getString(7), data.getString(6).toInt(), data.getString(2), "")
            tareasSQLite[tarea.id] = tarea
        }
    }

    fun cargarDatosFirebase() {
        tareasFirebase = ArrayMap()
        db.collection("tutores").document(correoUsuario)
                .collection("tareasAlimentos")
                .get()
                .addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        for (document in task.result!!) {
                            val tareaAdapter = document.toObject(TareaAdapter::class.java)
                            tareasFirebase[tareaAdapter.id] = tareaAdapter

                        }
                        db.collection("tutores").document(correoUsuario)
                                .collection("tareasMedicinas")
                                .get()
                                .addOnCompleteListener { task ->
                                    if (task.isSuccessful) {
                                        for (document in task.result!!) {
                                            val tareaAdapter = document.toObject(TareaAdapter::class.java)
                                            tareasFirebase[tareaAdapter.id] = tareaAdapter

                                        }
                                        db.collection("tutores").document(correoUsuario)
                                                .collection("tareasCitas")
                                                .get()
                                                .addOnCompleteListener { task ->
                                                    if (task.isSuccessful) {
                                                        for (document in task.result!!) {
                                                            val tareaAdapter = document.toObject(TareaAdapter::class.java)
                                                            tareasFirebase[tareaAdapter.id] = tareaAdapter

                                                        }
                                                        sincronizarSQLiteFirebase()
                                                    }
                                                }
                                    }
                                }
                    }
                }


    }

    private fun sincronizarSQLiteFirebase() {
        for (entryHijo in tareasFirebase) {
            if (!tareasSQLite.contains(entryHijo.key)) {
                if (entryHijo.value.fecha != "")
                    mDatabaseHelper!!.addData(entryHijo.key, entryHijo.value, entryHijo.value.categoria)
            }
        }
    }
}