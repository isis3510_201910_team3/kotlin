package com.devimpact.bhelp.bhelpkotlin.fragments.logic

import android.content.Context
import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.view.ViewPager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.devimpact.bhelp.bhelpkotlin.adapters.AdapterFragmentTabsHome
import com.devimpact.bhelp.bhelpkotlin.R


class FragmentHomeTabs : Fragment() {

    private val tabIcons = intArrayOf(R.drawable.ic_home_black_24dp, R.drawable.ic_menu_camera, R.drawable.ic_mic)

    private lateinit var viewPager: ViewPager
    private lateinit var tabs: TabLayout
    private lateinit var supportFragmentManager: FragmentManager
    lateinit var correoUsuario: String

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.activity_home, container, false)
        val sharedPref = activity!!.getSharedPreferences(getString(R.string.preference_file_key), Context.MODE_PRIVATE)
        correoUsuario = sharedPref.getString("correoUsuario","")

        supportFragmentManager = activity!!.supportFragmentManager

        viewPager = view.findViewById(R.id.home_viewpager_main)
        val fragmentAdapter = AdapterFragmentTabsHome(supportFragmentManager, correoUsuario)
        tabs = view.findViewById(R.id.home_tabs_main)

        viewPager.adapter = fragmentAdapter
        tabs.setupWithViewPager(viewPager)

        setupTabIcons()

        return view
    }

    private fun setupTabIcons() {
        tabs.getTabAt(0)!!.setIcon(tabIcons[0])
        tabs.getTabAt(1)!!.setIcon(tabIcons[1])
        tabs.getTabAt(2)!!.setIcon(tabIcons[2])
    }

    fun refresh(){
        for (i in supportFragmentManager.fragments.iterator()) {
            if (i.toString().contains("FragmentProfileHome")) {
                (i as FragmentProfileHome).refresh()
                return
            }

        }

        //var fragmentProfileHome = supportFragmentManager.findFragmentByTag("fragmentProfileHome") as FragmentProfileHome
        //fragmentProfileHome.refresh()
    }


}
