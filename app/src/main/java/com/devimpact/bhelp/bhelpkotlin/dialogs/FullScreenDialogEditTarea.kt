package com.devimpact.bhelp.bhelpkotlin.dialogs

import android.app.AlertDialog
import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.content.Context
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.*
import com.devimpact.bhelp.bhelpkotlin.models.Tarea
import java.text.SimpleDateFormat
import android.widget.EditText
import com.devimpact.bhelp.bhelpkotlin.databasehelpers.DatabaseHelper
import com.devimpact.bhelp.bhelpkotlin.fragments.agenda.AgendaFragmentInterface
import com.devimpact.bhelp.bhelpkotlin.fragments.agenda.FragmentAlimentos
import com.devimpact.bhelp.bhelpkotlin.fragments.agenda.FragmentCitas
import com.devimpact.bhelp.bhelpkotlin.fragments.agenda.FragmentMedicinas
import com.devimpact.bhelp.bhelpkotlin.services.NetworkUtil
import com.devimpact.bhelp.bhelpkotlin.R
import com.devimpact.bhelp.bhelpkotlin.models.Hijo
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.dialog_crear_tarea.*
import java.util.*


class FullScreenDialogEditTarea : DialogFragment() {

    private val db by lazy { FirebaseFirestore.getInstance() }
    private var fechaArg = ""
    private var horaArg = ""
    private lateinit var hijosList: ArrayList<Hijo>
    private lateinit var hijosListName: ArrayList<String>
    private lateinit var textFecha: TextView
    private lateinit var textTiempo: TextView
    private lateinit var editCategoria: Spinner
    private lateinit var editPrioridad: Spinner
    private lateinit var editTitulo: EditText
    private lateinit var editDescripcion: EditText
    private lateinit var editFecha: TextView
    private lateinit var editHora: TextView
    private lateinit var editHijo: Spinner
    private lateinit var buttonGuardar: TextView
    var mDatabaseHelper: DatabaseHelper? = null
    private lateinit var viejoTitulo: String
    private lateinit var viejoDescripcion: String
    private lateinit var viejoHijo: String
    lateinit var id: String
    private lateinit var tarea: Tarea
    private lateinit var supportFragmentManager: FragmentManager
    private lateinit var fragmentTareas: Fragment
    lateinit var correoUsuario: String
    private lateinit var instanceTimeCreacion: Calendar

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.dialog_crear_tarea, container, false)

        val fechaHoraArg = arguments!!.getString("fechaHora").split(" ")
        fechaArg = fechaHoraArg[0]
        horaArg = fechaHoraArg[1]

        iniciarHora(view)// Setear hora inicial
        iniciarFecha(view)// Setear fecha inicial
        val sharedPref = activity!!.getSharedPreferences(getString(R.string.preference_file_key), Context.MODE_PRIVATE)
        correoUsuario = sharedPref.getString("correoUsuario","")
        mDatabaseHelper = DatabaseHelper(view.context)



        val close = view.findViewById<ImageButton>(R.id.ic_close)
        buttonGuardar = view.findViewById(R.id.guardarTarea)
        editTitulo = view.findViewById(R.id.tituloTarea)
        editDescripcion = view.findViewById(R.id.descripcionTarea)
        editFecha = view.findViewById(R.id.fecha_text)
        editHora = view.findViewById(R.id.hora_text)
        editCategoria = view.findViewById(R.id.categoria)
        editPrioridad = view.findViewById(R.id.prioridad)
        editHijo = view.findViewById(R.id.spinner_hijo)

        viejoTitulo = arguments!!.getString("titulo")
        editTitulo.hint = viejoTitulo
        editTitulo.setText(viejoTitulo)

        val tab = sharedPref.getInt("actualTab", 0)
        editCategoria.setSelection(tab)


        supportFragmentManager = activity!!.supportFragmentManager
        for(i in supportFragmentManager.fragments.iterator())
        {
            if(i.toString().contains("FragmentAlimentos")  || i.toString().contains("FragmentMedicinas")  || i.toString().contains("FragmentCitas")) {
                fragmentTareas = i
            }
        }

        id = arguments!!.getString("id")

        viejoDescripcion = arguments!!.getString("descripcion")
        editDescripcion.hint = viejoDescripcion
        editDescripcion.setText(viejoDescripcion)

        viejoHijo = arguments!!.getString("hijo")

        hijosList = ArrayList()
        hijosListName = ArrayList()
        val ref = db.collection("tutores").document(correoUsuario)
                .collection("hijos")
        var hijo: Hijo
        var adapter: ArrayAdapter<String>

        ref.get()
                .addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        for (document in task.result!!) {
                            hijo = document.toObject(Hijo::class.java)
                            hijosList.add(hijo)
                            hijosListName.add(hijo.nombre)
                        }
                        adapter = ArrayAdapter(activity,
                                android.R.layout.simple_spinner_item, hijosListName)
                        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                        spinner_hijo.adapter = adapter

                        var hijoNombre: String
                        for(i in 0 until hijosListName.size){
                            hijoNombre = hijosListName[i]
                            if(hijoNombre == viejoHijo.split("-")[0]){
                                editHijo.setSelection(i)
                                break
                            }
                        }
                    }
                }

        close.setOnClickListener {
            dismiss()
        }
        buttonGuardar.setOnClickListener {
            if(validateFields()) {
                if (NetworkUtil.getConnectivityStatus(activity!!) == 0) {
                    makeDialog("Error de conexión", "En este momento no estás conectado a internet, la información se guardará localmente y cuando vuelvas a conectarte la sincronizaremos")
                }
                guardarTareaLocal()
                guardarTareaFirebase()
            }
            val categoria = editCategoria.selectedItem.toString()
            val fragment : AgendaFragmentInterface
            if(categoria == "Alimentos") {
                fragment = supportFragmentManager.findFragmentByTag("android:switcher:${fragmentTareas.id}:0") as FragmentAlimentos
            }
            else if(categoria == "Medicinas"){
                fragment = supportFragmentManager.findFragmentByTag("android:switcher:${fragmentTareas.id}:1") as FragmentMedicinas
            } else {
                fragment = supportFragmentManager.findFragmentByTag("android:switcher:${fragmentTareas.id}:2") as FragmentCitas
            }

            fragment.actualizar(true)

            val imm = activity!!.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(activity!!.currentFocus.windowToken, 0)
        }
        return view
    }


    private fun guardarTareaFirebase() {

        val categoria = editCategoria.selectedItem.toString()
        val tarea = Tarea(id, tarea.prioridad, tarea.titulo, tarea.descripcion, tarea.fecha, tarea.hora, 0)
        val coleccionPorCategoria = "tareas$categoria"

        val rootRef = FirebaseFirestore.getInstance()
        var ref = rootRef
                .collection("tutores").document(correoUsuario)
                .collection(coleccionPorCategoria)
        ref.document(id).update(
                "titulo", tarea.titulo,
                "descripcion", tarea.descripcion,
                "prioridad", tarea.prioridad,
                "fecha", tarea.fecha,
                "hora", tarea.hora
        ).addOnSuccessListener {
                    dismiss()
                }
                .addOnFailureListener { e ->
                    if (NetworkUtil.getConnectivityStatus(activity!!) !== 0) {
                        noGuarda()
                    }
                }
        ref = rootRef
                .collection("tutores").document(correoUsuario)
                .collection("hijos").document(viejoHijo)
                .collection("tareas")
        ref.document(tarea.id).set(tarea)
                .addOnFailureListener { e ->
                    if (NetworkUtil.getConnectivityStatus(context!!) !== 0) {
                        noGuarda()
                    }
                }

        if(tarea.prioridad == "Alta"){
            ref = rootRef
                    .collection("tutores").document(correoUsuario)
                    .collection("hijos").document(viejoHijo)
                    .collection("tareasPrioridadAlta")
            ref.document(tarea.id).set(tarea)
                    .addOnFailureListener { e ->
                        if (NetworkUtil.getConnectivityStatus(context!!) !== 0) {
                            noGuarda()
                        }
                    }
        }
    }

    private fun guardarTareaLocal() {

        val tarea = Tarea(id, tarea.prioridad, tarea.titulo, tarea.descripcion, tarea.fecha, tarea.hora, 0)

        mDatabaseHelper!!.updateTarea(id, tarea, viejoTitulo, editCategoria.selectedItem.toString()
        )
        dismiss()
    }

    private fun noGuarda() {
        makeDialog("Error creando tarea", "No se pudo crear la tarea, por favor vuelve a intentarlo")
        dismiss()
    }

    private fun iniciarHora(view: View) {
        textTiempo = view.findViewById(R.id.hora_text) as TextView
        val c = Calendar.getInstance()
        val format = SimpleDateFormat("HH:mm a", Locale.US)
        if(horaArg != ""){
            textTiempo!!.text = horaArg
        } else {
            textTiempo!!.text = format.format(c.time)
        }
        textTiempo!!.setOnClickListener { showTimePickerDialog() }
    }

    private fun iniciarFecha(view: View) {
        textFecha = view.findViewById(R.id.fecha_text) as TextView
        val c = Calendar.getInstance()
        val format = SimpleDateFormat("dd-MM-yyyy", Locale.US)
        if(fechaArg != ""){
            textFecha!!.text = fechaArg
        } else {
            textFecha!!.text = format.format(c.time)
        }
        textFecha!!.setOnClickListener { showDatePickerDialog() }
    }

    private fun showDatePickerDialog() {
        val c = Calendar.getInstance()
        val year = c.get(Calendar.YEAR)
        val month = c.get(Calendar.MONTH)
        val day = c.get(Calendar.DAY_OF_MONTH)

        val dpd = DatePickerDialog(activity, DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->

            c.set(year, monthOfYear, dayOfMonth)
            val format = SimpleDateFormat("dd-MM-yyyy", Locale.US)
            textFecha!!.text = format.format(c.time)

        }, year, month, day)

        dpd.datePicker.minDate = c.timeInMillis
        dpd.show()
    }

    private fun showTimePickerDialog() {
        val c = Calendar.getInstance()
        instanceTimeCreacion = c
        val hour = c.get(Calendar.HOUR_OF_DAY)
        val minute = c.get(Calendar.MINUTE)

        val tpd = TimePickerDialog(activity, TimePickerDialog.OnTimeSetListener(function = { view, h, m ->

            c.set(Calendar.HOUR_OF_DAY, h)
            c.set(Calendar.MINUTE, m)


            val format = SimpleDateFormat("HH:mm a", Locale.US)
            format.timeZone = (TimeZone.getTimeZone("GMT-05:00"))
            textTiempo!!.text = format.format(c.time)

        }),hour,minute,false)

        tpd.show()

    }


    private fun validateFields():Boolean{

        val titulo = editTitulo.text.toString().trim()
        val descripcion = editDescripcion.text.toString().trim()
        val fecha = editFecha.text.toString().trim()
        val hora = editHora.text.toString().trim()

        if (TextUtils.isEmpty(titulo) || titulo.isEmpty()) {
            editTitulo.error = "Por favor ingresa un título para la nueva tarea"
            return false
        }
        if (titulo.length !in 4..25) {
            editTitulo.error = "El título debe tener entre 4 y 25 caracteres"
            return false
        }
        if (descripcion.length > 35) {
            editDescripcion.error = "La descipción no puede tener más de 35 caracteres"
            return false
        }


        val idG = "$titulo-$fecha"

        tarea = Tarea(idG, editPrioridad.selectedItem.toString(), titulo, editDescripcion.text.toString(), fecha, hora, 0)

        return true
    }

    fun makeDialog(titulo:String, mensaje: String){
        val builder = AlertDialog.Builder(context)
        builder.setTitle(titulo)
        builder.setMessage(mensaje)
        builder.setPositiveButton("Aceptar") { _, _ ->
        }
        val dialog = builder.create()
        dialog.show()
    }
}


