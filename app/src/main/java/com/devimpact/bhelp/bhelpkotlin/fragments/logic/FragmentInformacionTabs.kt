package com.devimpact.bhelp.bhelpkotlin.fragments.logic

import android.content.Context
import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.view.ViewPager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.devimpact.bhelp.bhelpkotlin.R
import com.devimpact.bhelp.bhelpkotlin.adapters.AdapterFragmentTabsInformacion

class FragmentInformacionTabs : Fragment() {

    private val tabIcons = intArrayOf(R.drawable.ic_emergencias, R.drawable.ic_progreso, R.drawable.ic_medicinas)

    private lateinit var tabs: TabLayout
    private lateinit var supportFragmentManager: FragmentManager
    lateinit var correoUsuario: String

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_informacion_tabs, container, false)
        val sharedPref = activity!!.getSharedPreferences(getString(R.string.preference_file_key), Context.MODE_PRIVATE)
        correoUsuario = sharedPref.getString("correoUsuario", "")

        supportFragmentManager = activity!!.supportFragmentManager

        val fragmentAdapter = AdapterFragmentTabsInformacion(supportFragmentManager, correoUsuario)
        val viewPager = view.findViewById<ViewPager>(R.id.informacion_viewpager_main)
        tabs = view.findViewById(R.id.informacion_tabs_main)

        viewPager.adapter = fragmentAdapter
        tabs.setupWithViewPager(viewPager)

        setupTabIcons()

        return view
    }

    private fun setupTabIcons() {
        tabs.getTabAt(0)!!.setIcon(tabIcons[0])
        tabs.getTabAt(1)!!.setIcon(tabIcons[1])
        tabs.getTabAt(2)!!.setIcon(tabIcons[2])
    }
}