package com.devimpact.bhelp.bhelpkotlin.models

class Hijo(val id: String, val nombre:String, val sexo:Int, val peso: Int, val edad: Int, val fechaNacimiento:String,  val bebe: Int, val altura:Int, val compartido:Int, val vacunas: ArrayList<Vacuna>):Comparable<Hijo> {

    constructor() : this("", "",0, 0, 0, "", 0, 0,0, ArrayList())

    override fun compareTo(other: Hijo): Int {
        return this.nombre.compareTo(other.nombre)
    }

}
