package com.devimpact.bhelp.bhelpkotlin.adapters

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import com.devimpact.bhelp.bhelpkotlin.fragments.agenda.FragmentAlimentos
import com.devimpact.bhelp.bhelpkotlin.fragments.agenda.FragmentCitas
import com.devimpact.bhelp.bhelpkotlin.fragments.agenda.FragmentMedicinas

class AdapterFragmentTabsAgenda(fm: FragmentManager, correoUsuario: String) : FragmentPagerAdapter(fm) {

    private var cU = correoUsuario

    override fun getItem(position: Int): Fragment {
        return when (position) {
            0 -> {
                val fragmentAlimentos = FragmentAlimentos()
                val argsFH = Bundle()
                argsFH.putString("correo", cU)
                fragmentAlimentos.arguments = argsFH
                fragmentAlimentos
            }
            1 ->{
                val fragmentMedicinas = FragmentMedicinas()
                val argsFH = Bundle()
                argsFH.putString("correo", cU)
                fragmentMedicinas.arguments = argsFH
                fragmentMedicinas
            }
            else -> {
                val fragmentCitas = FragmentCitas()
                val argsFH = Bundle()
                argsFH.putString("correo", cU)
                fragmentCitas.arguments = argsFH
                fragmentCitas
            }
        }
    }

    override fun getCount(): Int {
        return 3
    }

    override fun getPageTitle(position: Int): CharSequence {
        return when (position) {
            0 -> "Alimentos"
            1 -> "Medicinas"
            else -> {
                return "Citas"
            }
        }
    }
}
