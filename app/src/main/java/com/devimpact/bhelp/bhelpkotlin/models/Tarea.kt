package com.devimpact.bhelp.bhelpkotlin.models


//class Tarea(val id:String?, val categoria:String, val prioridad:String, val titulo:String, val descripcion: String, val fecha: String, val hora: String)

class Tarea(val id:String, val prioridad:String, val titulo:String, val descripcion: String, val fecha: String, val hora: String, val compartido:Int)