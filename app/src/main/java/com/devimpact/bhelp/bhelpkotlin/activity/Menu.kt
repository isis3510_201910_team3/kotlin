package com.devimpact.bhelp.bhelpkotlin.activity

import android.Manifest
import android.app.AlertDialog
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.SharedPreferences
import android.net.ConnectivityManager
import android.os.Build
import android.os.Bundle
import android.support.design.widget.NavigationView
import android.support.v4.app.Fragment
import android.support.v4.view.GravityCompat
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import com.devimpact.bhelp.bhelpkotlin.R
import com.devimpact.bhelp.bhelpkotlin.threads.SincronizarHijosAsyncTask
import com.devimpact.bhelp.bhelpkotlin.threads.SincronizarTareasAsyncTask
import com.devimpact.bhelp.bhelpkotlin.fragments.logic.*
import com.devimpact.bhelp.bhelpkotlin.services.NetworkBroadcastReceiver
import com.devimpact.bhelp.bhelpkotlin.services.NetworkUtil
import com.devimpact.bhelp.bhelpkotlin.services.ShakeDetectorService
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import kotlinx.android.synthetic.main.activity_menu.*
import kotlinx.android.synthetic.main.app_bar_menu.*


class Menu : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {

    private lateinit var active: Fragment
    private lateinit var fragmentHomeTabs: FragmentHomeTabs
    private lateinit var fragmentInformacionTabs: FragmentInformacionTabs
    private lateinit var fragmentAgendaTabs: FragmentAgendaTabs
    private lateinit var fragmentConfiguraciones: FragmentConfiguraciones
    private lateinit var fragmentHijos: FragmentHijos
    private var channelId = ""
    private var channelName = ""
    private lateinit var notificationManager: NotificationManager
    private lateinit var sharedPref: SharedPreferences
    private lateinit var broadcastReceiver: NetworkBroadcastReceiver

    private var correoUsuario = ""
    private var desdeFragmentHijos = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        sharedPref = getSharedPreferences(getString(R.string.preference_file_key), Context.MODE_PRIVATE)
        correoUsuario = sharedPref.getString("correoUsuario","")

        SincronizarHijosAsyncTask(this, correoUsuario).start()
        SincronizarTareasAsyncTask(this, correoUsuario).start()

        setContentView(R.layout.activity_menu)
        setSupportActionBar(toolbar)
        installListener()

        if (intent.extras != null) {
            desdeFragmentHijos = intent.extras.getBoolean("desdeFragmentHijos")
        }


        val intent = Intent(this, ShakeDetectorService::class.java)
        //Start Service
        sharedPref.edit().putBoolean("desdeFragmentHijos", desdeFragmentHijos!!).commit()
        val startService = sharedPref.getBoolean(getString(R.string.emergency_call), true)

        if (startService) {
            val contact = sharedPref.getString(getString(R.string.emergency_call_contact), "")
            intent.putExtra(getString(R.string.emergency_call_contact), contact)
            intent.putExtra(getString(R.string.emergency_message_contact), contact)
            startService(intent)
        }

        if (NetworkUtil.getConnectivityStatus(applicationContext) == 0) {
            makeDialog("Error de conexión", "En este momento no estás conectado a internet, por lo que cargaremos la información local")
            sharedPref.edit().putBoolean(getString(R.string.user_notified_no_network), true).commit()
        }

        askPermissions()

        firebaseMessages()

        initializeFragments()

        val toggle = ActionBarDrawerToggle(
                this, drawer_layout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        drawer_layout.addDrawerListener(toggle)
        toggle.syncState()

        nav_view.setNavigationItemSelectedListener(this)
    }

    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            R.id.action_settings -> {
                val preferences = getPreferences(Context.MODE_PRIVATE)
                preferences.edit().putBoolean(getString(R.string.emergency_call), true).commit()
                if(sharedPref.getString("active", "") != ""){
                    supportFragmentManager.beginTransaction().hide(active).commit()
                    fragmentHomeTabs = supportFragmentManager.findFragmentByTag("fragmentHomeTabs")!! as FragmentHomeTabs
                    active = fragmentHomeTabs
                }
                sharedPref.edit().putString("active", "").commit()
                supportFragmentManager.beginTransaction().hide(active).show(fragmentConfiguraciones).commit()
                active = fragmentConfiguraciones
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        if(sharedPref.getString("active", "") != ""){
            active = supportFragmentManager.findFragmentByTag("fragmentHomeTabs")!!
        }
        sharedPref.edit().putString("active", "").commit()
        when (item.itemId) {
            R.id.nav_agenda -> {
                supportFragmentManager.beginTransaction().hide(active).show(fragmentAgendaTabs).commit()
                active = fragmentAgendaTabs
            }
            R.id.nav_informacion -> {
                supportFragmentManager.beginTransaction().hide(active).show(fragmentInformacionTabs).commit()
                active = fragmentInformacionTabs
            }
            R.id.nav_configuraciones -> {
                val preferences = getPreferences(Context.MODE_PRIVATE)
                preferences.edit().putBoolean(getString(R.string.emergency_call), true).commit()

                supportFragmentManager.beginTransaction().hide(active).show(fragmentConfiguraciones).commit()
                active = fragmentConfiguraciones
            }
            R.id.nav_hijos -> {
                val preferences = getPreferences(Context.MODE_PRIVATE)
                preferences.edit().putBoolean(getString(R.string.emergency_call), true).commit()

                supportFragmentManager.beginTransaction().hide(active).show(fragmentHijos).commit()
                active = fragmentHijos
            }
            R.id.nav_cerrar_sesion -> {
                // Build an AlertDialog
                val builder = AlertDialog.Builder(this)

                // Set a title for alert dialog
                builder.setTitle("Cerrar Sesión")

                // Ask the final question
                builder.setMessage("¿Deseas cerrar sesión?")

                // Set the alert dialog yes button click listener
                builder.setPositiveButton("Sí") { dialog, which ->
                    finish()
                    val intent = Intent(this, SplashScreenActivity::class.java)
                    sharedPref.edit().putString("correoUsuario", "").commit()
                    startActivity(intent)

                }

                // Set the alert dialog no button click listener
                builder.setNegativeButton("No") { dialog, which ->
                }

                val dialog = builder.create()
                // Display the alert dialog on interface
                dialog.show()
            }
        }

        drawer_layout.closeDrawer(GravityCompat.START)

        return true
    }

    private fun askPermissions() {
        Dexter.withActivity(this)
                .withPermissions(
                        Manifest.permission.CAMERA,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.CALL_PHONE,
                        Manifest.permission.RECORD_AUDIO,
                        Manifest.permission.SEND_SMS
                ).withListener(object : MultiplePermissionsListener {
                    override fun onPermissionsChecked(report: MultiplePermissionsReport) {/* ... */
                    }

                    override fun onPermissionRationaleShouldBeShown(permissions: List<PermissionRequest>, token: PermissionToken) {/* ... */
                        //show alert dialog with permission options
                        AlertDialog.Builder(this@Menu)
                                .setTitle(
                                        "Permissions Error!")
                                .setMessage(
                                        "Please allow")
                                .setNegativeButton(
                                        android.R.string.cancel
                                ) { dialog, _ ->
                                    dialog.dismiss()
                                    token?.cancelPermissionRequest()
                                }
                                .setPositiveButton(android.R.string.ok
                                ) { dialog, _ ->
                                    dialog.dismiss()
                                    token?.continuePermissionRequest()
                                }
                                .setOnDismissListener {
                                    token?.cancelPermissionRequest()
                                }
                                .show()
                    }

                }).check()
    }

    private fun initializeFragments() {

        fragmentConfiguraciones = FragmentConfiguraciones()

        fragmentHomeTabs = FragmentHomeTabs()

        fragmentAgendaTabs = FragmentAgendaTabs()

        fragmentHijos = FragmentHijos()

        fragmentInformacionTabs = FragmentInformacionTabs()



        supportFragmentManager.beginTransaction().add(R.id.containerFragment, fragmentHijos, "fragmentHijos").commit()
        active = fragmentHijos

        supportFragmentManager.beginTransaction().add(R.id.containerFragment, fragmentHomeTabs, "fragmentHomeTabs").hide(fragmentHomeTabs).commit()
        supportFragmentManager.beginTransaction().add(R.id.containerFragment, fragmentConfiguraciones, "fragmentConfiguraciones").hide(fragmentConfiguraciones).commit()
        supportFragmentManager.beginTransaction().add(R.id.containerFragment, fragmentAgendaTabs, "fragmentAgendaTabs").hide(fragmentAgendaTabs).commit()
        supportFragmentManager.beginTransaction().add(R.id.containerFragment, fragmentInformacionTabs, "fragmentInformacionTabs").hide(fragmentInformacionTabs).commit()

    }


    private fun firebaseMessages() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            // Create channel to show notifications.
            channelId = getString(R.string.default_notification_channel_id)
            channelName = getString(R.string.default_notification_channel_name)
            notificationManager = getSystemService(NotificationManager::class.java)
            notificationManager?.createNotificationChannel(NotificationChannel(channelId,
                    channelName, NotificationManager.IMPORTANCE_LOW))
        }

    }

    private fun installListener() {
        broadcastReceiver = NetworkBroadcastReceiver()
        registerReceiver(broadcastReceiver, IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION))
    }

    override fun onDestroy() {
        super.onDestroy()
        unregisterReceiver(broadcastReceiver)
    }

    fun makeDialog(titulo:String, mensaje: String){
        val builder = AlertDialog.Builder(this)
        builder.setTitle(titulo)
        builder.setMessage(mensaje)
        builder.setPositiveButton("Aceptar") { _, _ ->
        }
        val dialog = builder.create()
        dialog.show()
    }

}
