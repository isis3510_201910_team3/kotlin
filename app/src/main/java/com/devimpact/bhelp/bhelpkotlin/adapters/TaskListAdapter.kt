package com.devimpact.bhelp.bhelpkotlin.adapters

import android.app.*
import android.content.Context
import android.content.Intent
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentTransaction
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.devimpact.bhelp.bhelpkotlin.models.TareaAdapter
import android.os.Bundle
import android.widget.*
import com.devimpact.bhelp.bhelpkotlin.databasehelpers.DatabaseHelper
import com.devimpact.bhelp.bhelpkotlin.dialogs.FullScreenDialogEditTarea
import com.devimpact.bhelp.bhelpkotlin.R
import com.devimpact.bhelp.bhelpkotlin.services.NotificationBroadcastReceiver
import com.google.firebase.firestore.FirebaseFirestore
import java.text.SimpleDateFormat
import java.util.*


class TaskListAdapter(fragment:Fragment, tareas: ArrayList<TareaAdapter>, row_data: Int, correoUsuario: String) : RecyclerView.Adapter<TaskListAdapter.SingleItemRowHolder>(){

    private val db by lazy { FirebaseFirestore.getInstance() }
    private var itemsList: ArrayList<TareaAdapter> = tareas
    private var mContext: Fragment = fragment
    var layout: Int = row_data
    lateinit var titulo: TextView
    lateinit var fecha: TextView
    lateinit var prioridad: String
    lateinit var descripcion: TextView
    var mDatabaseHelper: DatabaseHelper? = null
    private var cU = correoUsuario
    private var idAlarma = 0

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): SingleItemRowHolder {
        val v = LayoutInflater.from(viewGroup.context).inflate(layout, null)
        mDatabaseHelper = DatabaseHelper(v.context)

        return SingleItemRowHolder(v)
    }

    fun borrarTarea(titulo:String, categoria: String, id: String, hijo: String){
        borrarLocal(titulo)
        borrarFirestore(titulo,categoria,id, hijo)
    }

    private fun borrarLocal(titulo: String){
        mDatabaseHelper!!.deleteName(titulo)
    }

    private fun borrarFirestore(titulo:String, categoria: String, id: String, hijo: String){
        val rootRef = FirebaseFirestore.getInstance()

        var ref = rootRef.collection("tutores").document(cU)
                .collection("tareas$categoria")
                .document(id)

        ref.delete()

        ref = rootRef
                .collection("tutores").document(cU)
                .collection("hijos").document(hijo)
                .collection("tareas")
                .document(id)

        ref.delete()


        if(prioridad == "Alta"){
            ref = rootRef
                    .collection("tutores").document(cU)
                    .collection("hijos").document(hijo)
                    .collection("tareasPrioridadAlta")
                    .document(id)

            ref.delete()

        }
    }

    override fun onBindViewHolder(holder: SingleItemRowHolder, i: Int) {

        val item = itemsList[i]

        prioridad = item.prioridad

        holder.descripcion.text = item.descripcion
        holder.titulo.tag = i
        holder.titulo.text = item.titulo
        holder.fecha.text = item.fecha
        holder.cat = item.categoria
        holder.id = item.id
        holder.hijo= item.hijo
        if(item.compartida == 1){
            holder.compartido.visibility = View.VISIBLE
        }

        when{
            item.prioridad.compareTo("Alta")==0 -> holder.prioridad.setBackgroundResource(R.drawable.ic_prioridad_alta)
            item.prioridad.compareTo("Media")==0 -> holder.prioridad.setBackgroundResource(R.drawable.ic_prioridad_media)
            item.prioridad.compareTo("Baja")==0 -> holder.prioridad.setBackgroundResource(R.drawable.ic_prioridad_baja)
        }

    }

    override fun getItemCount(): Int {
        return itemsList.size
    }

    inner class SingleItemRowHolder(view: View) : RecyclerView.ViewHolder(view) {

        var titulo: TextView = view.findViewById(R.id.txt_titulo)
        var fecha: TextView = view.findViewById(R.id.txt_fecha)
        var descripcion: TextView = view.findViewById(R.id.txt_descripcion)
        var prioridad: ImageView = view.findViewById(R.id.color_prioridad)
        private var checkTarea: CheckBox = view.findViewById(R.id.check_tarea)
        var edit: ImageButton = view.findViewById(R.id.editTarea)
        var compartido: ImageView = view.findViewById(R.id.compartidoTarea)
        lateinit var cat: String
        lateinit var id: String
        lateinit var hijo:String

        init {
            view.setOnClickListener { v ->
                val position = Integer.parseInt(titulo.tag.toString())
            }
            checkTarea?.setOnCheckedChangeListener { buttonView, isChecked ->
                if(isChecked){

                    // Build an AlertDialog
                    val builder = AlertDialog.Builder(view.context)

                    // Set a title for alert dialog
                    builder.setTitle("Eliminar tarea")

                    // Ask the final question
                    builder.setMessage("¿Deseas marcar esta tarea como compartido? Se eliminará permanentemente")

                    // Set the alert dialog yes button click listener
                    builder.setPositiveButton("Sí") { dialog, which ->
                        // Do something when user clicked the Yes button
                        // Set the TextView visibility GONE
                        //val position = Integer.parseInt(titulo.tag.toString())
                        borrarTarea(titulo.text.toString(), cat, id, hijo)
                        var fecha =fecha.text.toString()
                        deleteAlarm(fecha)
                        notifyDataSetChanged()
                    }

                    // Set the alert dialog no button click listener
                    builder.setNegativeButton("No") { dialog, which ->
                       checkTarea.isChecked = false
                    }

                    val dialog = builder.create()
                    // Display the alert dialog on interface
                    dialog.show()
                }
            }
            edit.setOnClickListener {
                crearFullScreenDialog(titulo.text.toString(), descripcion.text.toString(), fecha.text.toString(), id, hijo)
            }
        }
    }

    private fun crearFullScreenDialog( titulo: String, descripcion: String, fechaHora:String, id: String, hijo: String) {
        val fragmentManager = mContext.fragmentManager
        val newFragment = FullScreenDialogEditTarea()
        val args = Bundle()
        args.putString("hijo", hijo)
        args.putString("fechaHora", fechaHora)
        args.putString("prioridad", prioridad)
        args.putString("descripcion", descripcion)
        args.putString("titulo", titulo)
        args.putString("id", id)
        newFragment.arguments = args

        val transaction = fragmentManager!!.beginTransaction()
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
        transaction.add(android.R.id.content, newFragment, "FullScreenDialogEditTarea")
                .addToBackStack(null) //agregado para poder ir atrás
                .commit()
    }

    fun deleteAlarm(fecha: String) {

        val alarmMgr = mContext.activity!!.getSystemService(Context.ALARM_SERVICE) as AlarmManager

        val fechaHoraTarea = fecha
        val long = getTimeUntilDate(fechaHoraTarea)
        val broadcastIntent = Intent(mContext.activity, NotificationBroadcastReceiver::class.java)
        val pIntent = PendingIntent.getBroadcast(mContext.activity, idAlarma, broadcastIntent, 0)
        if (long > 0)
            alarmMgr.cancel(pIntent)
    }

    private fun getTimeUntilDate(dateParam: String): Long {

        //Current date
        val calendar = Calendar.getInstance()
        var actualDate = calendar.time
        val format = SimpleDateFormat("dd-MM-yyyy HH:mm a", Locale.US)
        val stringDate = format.format(actualDate)
        actualDate = format.parse(stringDate)

        //Date param
        val dueDate = format.parse(dateParam)

        calendar.time = dueDate
        idAlarma = ("" + calendar.get(Calendar.DAY_OF_MONTH) + calendar.get(Calendar.HOUR_OF_DAY) + calendar.get(Calendar.MINUTE) + calendar.get(Calendar.SECOND)).toInt()

        return dueDate.time - actualDate.time
    }
}

