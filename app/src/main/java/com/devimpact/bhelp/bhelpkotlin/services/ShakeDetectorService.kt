package com.devimpact.bhelp.bhelpkotlin.services

import android.annotation.SuppressLint
import android.app.Service
import android.content.Context
import android.content.Intent
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import android.net.Uri
import android.os.Handler
import android.os.IBinder
import android.telephony.SmsManager
import com.devimpact.bhelp.bhelpkotlin.R

class ShakeDetectorService : Service(), SensorEventListener  {

    private lateinit var  sensorManager : SensorManager
    private lateinit var sensor : Sensor
    private var accelAppartGravity : Float = 0.0f
    private var currentAccelWithGravity : Float = 0.0f
    private var lastAccelGravity : Float = 0.0f
    private var messageContact = ""
    private var callContact = ""
    private var executeService = false


    override fun onBind(intent: Intent?): IBinder? {
        return null
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        messageContact = intent!!.getStringExtra(getString(R.string.emergency_message_contact))
        callContact = intent!!.getStringExtra(getString(R.string.emergency_call_contact))

        executeService = intent!!.getBooleanExtra(getString(R.string.dont_execute_service), false)

        sensorManager = getSystemService(Context.SENSOR_SERVICE) as SensorManager
        sensor = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER)
        sensorManager.registerListener(this, sensor, SensorManager.SENSOR_DELAY_UI, Handler())
        return START_STICKY
    }

    override fun onAccuracyChanged(sensor: Sensor?, accuracy: Int) {
    }

    @SuppressLint("MissingPermission")
    override fun onSensorChanged(event: SensorEvent?) {
        val x = event!!.values[0]
        val y = event!!.values[0]
        val z = event!!.values[0]
        lastAccelGravity = currentAccelWithGravity
        currentAccelWithGravity = Math.sqrt((x * x + y * y + z * z).toDouble()).toFloat()
        val delta = currentAccelWithGravity - lastAccelGravity
        accelAppartGravity = accelAppartGravity * 0.9f + delta

        if(!executeService) {
            if (accelAppartGravity > 50) {
                if (messageContact != "")
                    SmsManager.getDefault().sendTextMessage(messageContact, null, "BHelp ha registrado una posible emergencia con Maria, comunicate con Vivian en cuanto puedas", null, null)

                val llamada = "tel:$callContact"
                if (callContact != "") {
                    val intent = Intent(Intent.ACTION_CALL, Uri.parse(llamada))
                    intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                    startActivity(intent)
                }

            }
        } else {
            sensorManager.unregisterListener(this)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        sensorManager.unregisterListener(this)
    }

}