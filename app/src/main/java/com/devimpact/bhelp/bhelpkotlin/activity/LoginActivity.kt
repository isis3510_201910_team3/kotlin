package com.devimpact.bhelp.bhelpkotlin.activity

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.annotation.TargetApi
import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.support.v7.app.AppCompatActivity
import android.os.Build
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import com.devimpact.bhelp.bhelpkotlin.R

import kotlinx.android.synthetic.main.activity_login.*
import android.content.Intent
import android.widget.Button
import android.content.IntentFilter
import android.net.ConnectivityManager
import android.widget.EditText
import com.devimpact.bhelp.bhelpkotlin.services.NetworkUtil
import com.devimpact.bhelp.bhelpkotlin.services.NetworkBroadcastReceiver
import com.google.firebase.auth.FirebaseAuth


class LoginActivity : AppCompatActivity() {

    private lateinit var dialog: Dialog
    private lateinit var editCorreo: EditText
    private lateinit var editContra: EditText
    private lateinit var firebaseAuth: FirebaseAuth
    private lateinit var broadcastReceiver: NetworkBroadcastReceiver

    private var email = ""
    private var password = ""

    private lateinit var emailRecup: EditText



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        installListener()

        editCorreo = findViewById(R.id.email)
        editContra = findViewById(R.id.password)

        firebaseAuth = FirebaseAuth.getInstance()

        email_sign_in_button.setOnClickListener {
            if (validateFields()) {
                showProgress(true)
                if (NetworkUtil.getConnectivityStatus(this!!) == 0) {
                    makeDialog("Error de conexión", "En este momento no estás conectado a internet, no es posible hacer el login. Por favor revisa tu conexión y vuelve a intentarlo.")
                    showProgress(false)
                }
                else {
                    loguearUsuario()
                }
            }
        }
        registrarse.setOnClickListener {
            val registro = Intent(this, RegistroActivity::class.java)
            startActivity(registro)
        }

        olvido_contra.setOnClickListener {
                    dialog = Dialog(this)
                    dialog.setContentView(R.layout.dialog_reset_password)

                    emailRecup = dialog.findViewById(R.id.edtResetEmail)
                    val cancelar = dialog.findViewById<Button>(R.id.btnBack)
                    val guardar = dialog.findViewById<Button>(R.id.btnResetPassword)


                    cancelar.setOnClickListener {
                        dialog.dismiss()
                        //finish()
                    }
                    guardar.setOnClickListener {


                        if (TextUtils.isEmpty(emailRecup.text.toString().trim())) {
                            emailRecup.error = "Por favor, ingresa el correo"
                        } else {
                            firebaseAuth!!.sendPasswordResetEmail(emailRecup.text.toString().trim())
                                    .addOnCompleteListener { task ->
                                        if (task.isSuccessful) {
                                            makeDialog("Error recuperar contraseña", "Revisa tu correo para recuperar tu contraseña.")
                                            dialog.dismiss()
                                        } else {
                                            makeDialog("Error recuperar contraseña", "No se pudo recuperar, "+task.exception!!.message+" con "+emailRecup.text.toString().trim())
                                        }
                                    }
                        }
                    }

                    dialog.window.setBackgroundDrawableResource(R.drawable.round_background)

                    dialog.setCancelable(true)

                    dialog.show()
                }
    }

    private fun loguearUsuario() {

        firebaseAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener { task ->
                    //checking if success
                    if (task.isSuccessful) {
                        val pos = email.indexOf("@")
                        val user = email.substring(0, pos)
                        val sharedPref = getSharedPreferences(getString(R.string.preference_file_key), Context.MODE_PRIVATE)
                        sharedPref.edit().putString("correoUsuario", email).commit()
                        val principal = Intent(this, Menu::class.java)
                        finish()
                        startActivity(principal)

                    } else {
                        makeDialog("Error iniciar sesión", "Correo o contraseña inválidos. Verifica e intenta de nuevo.")
                        showProgress(false)
                    }
                }
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private fun showProgress(show: Boolean) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            val shortAnimTime = resources.getInteger(android.R.integer.config_shortAnimTime).toLong()

            login_form.visibility = if (show) View.GONE else View.VISIBLE
            login_form.animate()
                    .setDuration(shortAnimTime)
                    .alpha((if (show) 0 else 1).toFloat())
                    .setListener(object : AnimatorListenerAdapter() {
                        override fun onAnimationEnd(animation: Animator) {
                            login_form.visibility = if (show) View.GONE else View.VISIBLE
                        }
                    })

            login_progress.visibility = if (show) View.VISIBLE else View.GONE
            login_progress.animate()
                    .setDuration(shortAnimTime)
                    .alpha((if (show) 1 else 0).toFloat())
                    .setListener(object : AnimatorListenerAdapter() {
                        override fun onAnimationEnd(animation: Animator) {
                            login_progress.visibility = if (show) View.VISIBLE else View.GONE
                        }
                    })
        } else {
            login_progress.visibility = if (show) View.VISIBLE else View.GONE
            login_form.visibility = if (show) View.GONE else View.VISIBLE
        }
    }

    private fun validateFields(): Boolean {

        email = editCorreo!!.text.toString().trim { it <= ' ' }
        password = editContra!!.text.toString().trim { it <= ' ' }

        if (TextUtils.isEmpty(email)) {
            editCorreo.error = "Se debe ingresar un email"
            return false
        }

        if (email.length !in 5..30) {
            editCorreo.error = "El correo debe tener entre 5 y 25 caracteres"
            return false
        }

        if(!email.contains("@"))
        {
            editCorreo.error = "Se debe ingresar un email válido(@gmail, homail,etc)"
            return false
        }

        if (TextUtils.isEmpty(password)) {
            editContra.error = "Falta ingresar la contraseña"
            return false
        }

        if(password.length < 6){
            editContra.error = "La contraseña debe tener mínimo 6 caracteres"
            return false
        }

        if (password.length !in 6..20) {
            editContra.error = "La contraseña debe tener entre 6 y 20 caracteres"
            return false
        }

        return true
    }

    private fun installListener() {
        broadcastReceiver = NetworkBroadcastReceiver()
        registerReceiver(broadcastReceiver, IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION))
    }

    override fun onDestroy() {
        super.onDestroy()
        unregisterReceiver(broadcastReceiver)
    }

    fun makeDialog(titulo:String, mensaje: String){
        val builder = AlertDialog.Builder(this)
        builder.setTitle(titulo)
        builder.setMessage(mensaje)
        builder.setPositiveButton("Aceptar") { _, _ ->
        }
        val dialog = builder.create()
        dialog.show()
    }
}
