package com.devimpact.bhelp.bhelpkotlin.fragments.logic

import android.os.Bundle
import android.provider.ContactsContract
import android.content.Intent
import android.database.Cursor
import android.app.Activity
import android.app.AlertDialog
import android.app.Dialog
import android.content.ContentResolver
import android.content.Context
import android.support.v4.app.Fragment
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.devimpact.bhelp.bhelpkotlin.R
import com.devimpact.bhelp.bhelpkotlin.services.NetworkUtil
import com.devimpact.bhelp.bhelpkotlin.services.ShakeDetectorService
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.app_bar_menu.*


class FragmentConfiguraciones : Fragment() {

    private val SELECT_CALL_PHONE_NUMBER: Int = 123
    private val SELECT_MESSAGE_PHONE_NUMBER: Int = 321

    private lateinit var callContactText: TextView
    private lateinit var messageContactText: TextView

    private lateinit var contentResolver: ContentResolver

    private lateinit var switchRecordatorios: Switch
    private lateinit var switchLlamadas: Switch

    private lateinit var compartirTareasHijos: TextView
    private lateinit var email: EditText

    private lateinit var dialog: Dialog

    private val stringCallContacto = "Tu contacto es: "
    lateinit var correoUsuario: String

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_configuraciones, container, false)
        val sharedPref = activity!!.getSharedPreferences(getString(R.string.preference_file_key), Context.MODE_PRIVATE)
        correoUsuario = sharedPref.getString("correoUsuario","")

        val selectMessageContact = view.findViewById<TextView>(R.id.contactoMensaje)

        selectMessageContact.setOnClickListener {
            val i = Intent(Intent.ACTION_PICK)
            i.type = ContactsContract.CommonDataKinds.Phone.CONTENT_TYPE
            startActivityForResult(i, SELECT_MESSAGE_PHONE_NUMBER)
        }

        val selectCallContact = view.findViewById<TextView>(R.id.contactoLlamada)

        selectCallContact.setOnClickListener {
            val i = Intent(Intent.ACTION_PICK)
            i.type = ContactsContract.CommonDataKinds.Phone.CONTENT_TYPE
            startActivityForResult(i, SELECT_CALL_PHONE_NUMBER)
        }

        initializeContacts(view)

        switchRecordatorios = view.findViewById(R.id.switchRecordatorios)
        switchRecordatorios.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                //TODO fuck
            } else {
                //TODO fuck
            }
        }
        switchLlamadas = view.findViewById(R.id.switchLlamada)

        switchLlamadas.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                restartService(false)
            } else {
                restartService(true)
            }
        }

        contentResolver = activity!!.contentResolver


        compartirTareasHijos = view.findViewById(R.id.compartirHijo)

        compartirTareasHijos.setOnClickListener {
            dialog = Dialog(activity)
            dialog.setContentView(R.layout.dialog_compartir)

            email = dialog.findViewById(R.id.emailTutor)
            val cancelar = dialog.findViewById<Button>(R.id.cancelar)
            val guardar = dialog.findViewById<Button>(R.id.compartir)


            cancelar.setOnClickListener {
                dialog.dismiss()
            }
            guardar.setOnClickListener {
                if (validateFields()) {
                    agregarCorreo(email.text.toString())
                    dialog.dismiss()
                }
            }

            dialog.window.setBackgroundDrawableResource(R.drawable.round_background)

            dialog.setCancelable(false)

            dialog.show()
        }

        return view
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode === Activity.RESULT_OK) {
            when (requestCode) {
                SELECT_CALL_PHONE_NUMBER -> {
                    var cursor: Cursor? = null
                    try {
                        var phoneNo: String? = null
                        var name: String? = null

                        val uri = data!!.data
                        cursor = contentResolver.query(uri, null, null, null, null)
                        cursor!!.moveToFirst()
                        val phoneIndex = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER)
                        val nameIndex = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME)
                        phoneNo = cursor.getString(phoneIndex)
                        name = cursor.getString(nameIndex)

                        val preferences = activity!!.getPreferences(Context.MODE_PRIVATE)
                        preferences.edit().putString(getString(R.string.emergency_call_contact), phoneNo).commit()
                        preferences.edit().putString(getString(R.string.emergency_call_contact_name), name).commit()
                        callContactText.text = "$stringCallContacto$name"

                        cursor.close()

                        restartService(false)

                    } catch (e: Exception) {
                        e.printStackTrace()
                    }

                }
                SELECT_MESSAGE_PHONE_NUMBER -> {
                    var cursor: Cursor? = null
                    try {
                        var phoneNo: String? = null
                        var name: String? = null

                        val uri = data!!.data
                        cursor = contentResolver.query(uri, null, null, null, null)
                        cursor!!.moveToFirst()
                        val phoneIndex = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER)
                        val nameIndex = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME)
                        phoneNo = cursor.getString(phoneIndex)
                        name = cursor.getString(nameIndex)

                        val preferences = activity!!.getPreferences(Context.MODE_PRIVATE)
                        preferences.edit().putString(getString(R.string.emergency_message_contact), phoneNo).commit()
                        preferences.edit().putString(getString(R.string.emergency_message_contact_name), name).commit()
                        messageContactText.text = "$stringCallContacto$name"

                        cursor.close()
                        restartService(false)

                    } catch (e: Exception) {
                        e.printStackTrace()
                    }

                }
            }
        } else {
        }
    }

    private fun restartService(killed: Boolean) {
        val intent = Intent(activity, ShakeDetectorService::class.java)

        val sharedPref = activity!!.getPreferences(Context.MODE_PRIVATE)
        val defaultStartService = true
        val startService = sharedPref.getBoolean(getString(R.string.emergency_call), defaultStartService)


        if (startService) {
            val contactCall = sharedPref.getString(getString(R.string.emergency_call_contact), "")
            val contactMessage = sharedPref.getString(getString(R.string.emergency_message_contact), "")
            if (killed)
                intent.putExtra(getString(R.string.dont_execute_service), true)

            intent.putExtra(getString(R.string.emergency_call_contact), contactCall)
            intent.putExtra(getString(R.string.emergency_message_contact), contactMessage)
            activity!!.startService(intent)
        }
    }

    private fun initializeContacts(view: View) {
        val sharedPref = activity!!.getPreferences(Context.MODE_PRIVATE)
        val contactLlamada = sharedPref.getString(getString(R.string.emergency_call_contact_name), "")
        val contactMensaje = sharedPref.getString(getString(R.string.emergency_message_contact_name), "")

        callContactText = view.findViewById(R.id.contactoLlamada)
        messageContactText = view.findViewById(R.id.contactoMensaje)

        if (contactLlamada != "") {
            callContactText.text = "$stringCallContacto$contactLlamada"
        }
        else
        {
            callContactText.text = "Selecciona contacto llamada de emergencia"
        }

        if (contactMensaje != "") {
            messageContactText.text = "$stringCallContacto$contactMensaje"
        }
        else{
            messageContactText.text = "Selecciona contacto mensaje emergencia"
        }
    }

    private fun validateFields(): Boolean {

        if (TextUtils.isEmpty(email.text)) {
            email.error = "Se debe ingresar un email"
            return false
        }

        if (!email.text.contains("@")) {
            email.error = "Se debe ingresar un email válido(@gmail, homail,etc)"
            return false
        }
        return true
    }

    private fun agregarCorreo(correo: String) {

        val rootRef = FirebaseFirestore.getInstance()
        val ref = rootRef
                .collection("tutores")

        val compartir = HashMap<String, String>()
        compartir["compartido"] = correo

        ref.document(correoUsuario).update(compartir as Map<String, Any>)
                .addOnFailureListener { e ->
                    if (NetworkUtil.getConnectivityStatus(containerFragment.context) !== 0) {
                        makeDialog("Error de conexión", "No se pudo compartir, por favor vuelve a intentarlo más tarde")
                    }
                }
                .addOnCompleteListener {
                }

    }

    fun makeDialog(titulo:String, mensaje: String){
        val builder = AlertDialog.Builder(context)
        builder.setTitle(titulo)
        builder.setMessage(mensaje)
        builder.setPositiveButton("Aceptar") { _, _ ->
        }
        val dialog = builder.create()
        dialog.show()
    }
}
