package com.devimpact.bhelp.bhelpkotlin.threads

import android.util.ArrayMap
import com.devimpact.bhelp.bhelpkotlin.databasehelpers.DatabaseHelperHijos
import com.devimpact.bhelp.bhelpkotlin.activity.Menu
import com.devimpact.bhelp.bhelpkotlin.services.NetworkUtil
import com.devimpact.bhelp.bhelpkotlin.models.Hijo
import com.google.firebase.firestore.FirebaseFirestore
import java.lang.ref.WeakReference

class SincronizarHijosAsyncTask(context: Menu, correo: String) : Thread() {

    private val db by lazy { FirebaseFirestore.getInstance() }

    private var resp: String? = null
    private var correoUsuario = correo
    private val activityReference: WeakReference<Menu> = WeakReference(context)
    private var mDatabaseHelperHijos: DatabaseHelperHijos? = null
    private lateinit var hijosSQLite: ArrayMap<String, Hijo>
    private lateinit var hijosFirebase: ArrayMap<String, Hijo>

    override fun run() {
        super.run()
        try {
            val activity = activityReference.get()
            mDatabaseHelperHijos = DatabaseHelperHijos(activity!!.applicationContext)
            cargarDatosSQLite()

            if (NetworkUtil.getConnectivityStatus(activity!!.applicationContext) != 0) {
                cargarDatosFirebase()
            }

        } catch (e: InterruptedException) {
            e.printStackTrace()
            resp = e.message
        } catch (e: Exception) {
            e.printStackTrace()
            resp = e.message
        }

    }

    private fun cargarDatosSQLite() {
        hijosSQLite = ArrayMap()
        val data = mDatabaseHelperHijos!!.getDatos()
        while (data!!.moveToNext()) {
            val id = data.getString(0)
            hijosSQLite[id] = Hijo(id, data.getString(1), Integer.parseInt(data.getString(2)), Integer.parseInt(data.getString(3)), Integer.parseInt(data.getString(4)), data.getString(8), Integer.parseInt(data.getString(5)), Integer.parseInt(data.getString(6)), Integer.parseInt(data.getString(7)), ArrayList())
        }
    }

    fun cargarDatosFirebase() {
        hijosFirebase = ArrayMap()
        db.collection("tutores").document(correoUsuario)
                .collection("hijos")
                .get()
                .addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        for (document in task.result!!) {
                            val id = document.get("id").toString()
                            hijosFirebase[id] = document.toObject(Hijo::class.java)

                            var bebe = when (hijosFirebase[id]!!.edad > 3 * 12) {
                                true -> 0
                                false -> 1
                            }

                        }
                    }
                }

        var correoCompartido = ""
        db.collection("tutores").document(correoUsuario).get()
                .addOnCompleteListener { result ->
                    if (result.isSuccessful) {
                        correoCompartido = result.result!!.get("compartido").toString()
                    }
                    db.collection("tutores").document(correoCompartido)
                            .collection("hijos")
                            .get()
                            .addOnCompleteListener { task ->
                                if (task.isSuccessful) {
                                    for (document in task.result!!) {
                                        val id = document.get("id").toString()
                                        hijosFirebase[id] = document.toObject(Hijo::class.java)
                                    }
                                    sincronizarSQLiteFirebase()
                                }
                            }
                }

    }

    private fun sincronizarSQLiteFirebase() {
        for (entryHijo in hijosFirebase) {
            if (!hijosSQLite.contains(entryHijo.key)) {
                mDatabaseHelperHijos!!.addData(entryHijo.key, entryHijo.value)
            }
        }
        for (entryHijo in hijosSQLite) {
            if (!hijosFirebase.contains(entryHijo.key)) {
                mDatabaseHelperHijos!!.deleteName(entryHijo.key)
            }
        }
    }
}