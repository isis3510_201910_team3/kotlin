package com.devimpact.bhelp.bhelpkotlin.fragments.logic

import android.app.SearchManager
import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.SearchView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.devimpact.bhelp.bhelpkotlin.databasehelpers.DatabaseHelper
import com.devimpact.bhelp.bhelpkotlin.databasehelpers.DatabaseHelperHijos
import com.devimpact.bhelp.bhelpkotlin.R
import com.devimpact.bhelp.bhelpkotlin.adapters.InfoListAdapter
import com.devimpact.bhelp.bhelpkotlin.models.Info
//import com.devimpact.bhelp.bhelpkotlin.R.id.sinPrioritarias
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.fragment_informacion.view.*
import kotlin.collections.ArrayList

class FragmentSalud : Fragment() , InfoListAdapter.ItemClickListener {
    override fun onItemClicked(info: Info) {

    }

    private var recyclerView: RecyclerView? = null
    private var infoList: MutableList<Info>? = null
    private var mAdapter: InfoListAdapter? = null
    private var searchView: SearchView? = null

    private val db by lazy { FirebaseFirestore.getInstance() }
    private lateinit var textCategoria: TextView
    private lateinit var textCategoriaInfo: TextView

    lateinit var correoUsuario: String

    var mDatabaseHelper: DatabaseHelper? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_informacion, container, false)
        textCategoria = view.findViewById(R.id.textCategoria)
        textCategoria.text = "Salud"
        textCategoriaInfo = view.findViewById(R.id.textCategoriaInfo)
        textCategoriaInfo.text = "Busca información sobre la salud de tu hijo"

        recyclerView = view.infoView
        infoList = ArrayList()
        mAdapter = InfoListAdapter(infoList as ArrayList, this)

        val layoutManager = LinearLayoutManager(context)
        recyclerView!!.layoutManager = layoutManager
        recyclerView!!.adapter = mAdapter

        val searchManager = activity!!.getSystemService(Context.SEARCH_SERVICE) as SearchManager
        searchView = view.action_search
        searchView!!.maxWidth = Integer.MAX_VALUE

        searchView!!.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String): Boolean {
                // filter recycler view when query submitted
                mAdapter!!.filter.filter(query)
                return false
            }

            override fun onQueryTextChange(query: String): Boolean {
                // filter recycler view when text is changed
                mAdapter!!.filter.filter(query)
                return false
            }
        })

        cargarDatosFirebase()

        return view
    }

    fun refresh(){
    }

    fun cargarDatosFirebase() {
        db.collection("salud")
                .get()
                .addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        for (document in task.result!!) {
                            infoList!!.add(Info(document.get("tema").toString(), document.get("informacion").toString()))
                        }
                        mAdapter = InfoListAdapter(infoList as ArrayList, this)
                        recyclerView!!.adapter = mAdapter
                    }
                }
                .addOnFailureListener { e ->
                }
    }

}
