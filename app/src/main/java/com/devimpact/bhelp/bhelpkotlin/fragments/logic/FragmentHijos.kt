package com.devimpact.bhelp.bhelpkotlin.fragments.logic

import android.app.AlertDialog
import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentTransaction
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.devimpact.bhelp.bhelpkotlin.R
import com.devimpact.bhelp.bhelpkotlin.adapters.HijoListAdapter
import com.devimpact.bhelp.bhelpkotlin.databasehelpers.DatabaseHelperHijos
import com.devimpact.bhelp.bhelpkotlin.dialogs.FullScreenDialogCrearHijo
import com.devimpact.bhelp.bhelpkotlin.models.Hijo
import com.devimpact.bhelp.bhelpkotlin.models.Vacuna
import com.devimpact.bhelp.bhelpkotlin.services.NetworkUtil
import com.google.firebase.firestore.FirebaseFirestore

class FragmentHijos : Fragment() {

    private val db by lazy { FirebaseFirestore.getInstance() }
    private lateinit var hijosView: RecyclerView
    private lateinit var hijosListAdapter: HijoListAdapter
    private lateinit var hijosList: ArrayList<Hijo>
    lateinit var sinHijos: ImageView
    private lateinit var mensaje: TextView
    private lateinit var mensje: TextView

    private var mDatabaseHelperHijos: DatabaseHelperHijos? = null
    lateinit var supportFragmentManager: FragmentManager
    lateinit var correoUsuario: String

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val view = inflater.inflate(R.layout.fragment_hijos, container, false)
        val sharedPref = activity!!.getSharedPreferences(getString(R.string.preference_file_key), Context.MODE_PRIVATE)
        correoUsuario = sharedPref.getString("correoUsuario", "")

        supportFragmentManager = activity!!.supportFragmentManager
        sinHijos = view.findViewById(R.id.sinHijos)
        mensaje = view!!.findViewById(R.id.txt_sinHijos)
        mensje = view!!.findViewById(R.id.txt_sinHijos2)

        hijosList = ArrayList()
        hijosView = view.findViewById(R.id.hijosView)
        hijosView.layoutManager = GridLayoutManager(this.activity, 1)
        mDatabaseHelperHijos = DatabaseHelperHijos(view.context)

        if (NetworkUtil.getConnectivityStatus(view.context) == 0) {
            cargarDatosSQLite()
            val userNotified = sharedPref.getBoolean(getString(R.string.user_notified_no_network), false)
            if (!userNotified) {
                makeDialog("Error de conexión", "En este momento no estás conectado a internet, por lo que cargaremos la información local")
                sharedPref.edit().putBoolean(getString(R.string.user_notified_no_network), true).commit()
            }
        } else {
            cargarDatosFirebase()
            sharedPref.edit().putBoolean(getString(R.string.user_notified_no_network), false).commit()
        }
        actualizar(false)

        val recordButton = view.findViewById<android.support.design.widget.FloatingActionButton>(R.id.addHijo)

        recordButton.setOnClickListener {
            crearFullScreenDialog()
            actualizar(true)
        }

        return view
    }

    fun cargarDatosFirebase() {
        hijosList = ArrayList()
        var vacunasHijo = ArrayList<Vacuna>()
        val ref = db.collection("tutores").document(correoUsuario)
                .collection("hijos")
        var hijo: Hijo

        ref.get()
                .addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        for (document in task.result!!) {
                            hijo = document.toObject(Hijo::class.java)
                            hijosList.add(hijo)
                        }
                    }
                    updateView()
                }

        var correoCompartido = ""
        db.collection("tutores").document(correoUsuario).get()
                .addOnCompleteListener { result ->
                    if (result.isSuccessful) {
                        correoCompartido = result.result!!.get("compartido").toString()
                    }
                    db.collection("tutores").document(correoCompartido)
                            .collection("hijos")
                            .get()
                            .addOnCompleteListener { task ->
                                if (task.isSuccessful) {
                                    for (document in task.result!!) {
                                        hijo = document.toObject(Hijo::class.java)
                                        hijosList.add(hijo)
                                    }
                                    hijosList.sort()
                                    hijosListAdapter = HijoListAdapter(this, hijosList, R.layout.item_hijo, correoUsuario)
                                    hijosView.adapter = hijosListAdapter
                                }
                                updateView()
                            }
                }


    }


    private fun cargarDatosSQLite() {
        hijosList = ArrayList()
        val data = mDatabaseHelperHijos!!.getDatos()
        while (data!!.moveToNext()) {
            hijosList.add(Hijo(data.getString(0), data.getString(1), Integer.parseInt(data.getString(2)), Integer.parseInt(data.getString(3)), Integer.parseInt(data.getString(4)), data.getString(8), Integer.parseInt(data.getString(5)), Integer.parseInt(data.getString(6)), Integer.parseInt(data.getString(7)), ArrayList()))
        }
        hijosList.sort()
        updateView()
        hijosListAdapter = HijoListAdapter(this, hijosList, R.layout.item_hijo, correoUsuario)
        hijosView.adapter = hijosListAdapter
    }

    fun actualizar(refreshData: Boolean) {

        if (refreshData) {
            hijosList = ArrayList()
            if (NetworkUtil.getConnectivityStatus(activity!!.applicationContext) == 0) {
                cargarDatosSQLite()
            } else {
                cargarDatosFirebase()
            }
        }
        hijosListAdapter = HijoListAdapter(this, hijosList, R.layout.item_hijo, correoUsuario)
        hijosView.adapter = hijosListAdapter
        registerForContextMenu(hijosView)
    }


    private fun crearFullScreenDialog() {
        val fragmentManager = supportFragmentManager
        val newFragment = FullScreenDialogCrearHijo()
        val argsCT = Bundle()
        argsCT.putString("correo", correoUsuario)
        newFragment.arguments = argsCT

        val shared = activity!!.getPreferences(Context.MODE_PRIVATE)
        shared.edit().putBoolean("desdeProfileHome", false).commit()

        val transaction = fragmentManager.beginTransaction()
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
        transaction.add(android.R.id.content, newFragment, "FullScreenDialogCrearHijo")
                .addToBackStack(null)
                .commit()
    }

    private fun updateView() {
        if (hijosList.isEmpty()) {
            hijosView.visibility = View.GONE
            sinHijos.visibility = View.VISIBLE
            mensaje.visibility = View.VISIBLE
            mensje.visibility = View.VISIBLE
        } else {
            hijosView.visibility = View.VISIBLE
            sinHijos.visibility = View.GONE
            mensaje.visibility = View.GONE
            mensje.visibility = View.GONE
        }
    }

    fun makeDialog(titulo:String, mensaje: String){
        val builder = AlertDialog.Builder(context)
        builder.setTitle(titulo)
        builder.setMessage(mensaje)
        builder.setPositiveButton("Aceptar") { _, _ ->
        }
        val dialog = builder.create()
        dialog.show()
    }
}
