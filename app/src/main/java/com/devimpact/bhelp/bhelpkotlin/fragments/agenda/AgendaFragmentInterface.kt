package com.devimpact.bhelp.bhelpkotlin.fragments.agenda

import android.app.AlertDialog
import android.content.Context
import android.support.v4.app.Fragment
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.devimpact.bhelp.bhelpkotlin.databasehelpers.DatabaseHelper
import com.devimpact.bhelp.bhelpkotlin.R
import com.devimpact.bhelp.bhelpkotlin.models.Tarea
import com.devimpact.bhelp.bhelpkotlin.models.TareaAdapter
import com.google.firebase.firestore.FirebaseFirestore
import kotlin.collections.ArrayList

abstract class AgendaFragmentInterface : Fragment() {

    lateinit var tareasList: ArrayList<TareaAdapter>
    var tareas = ArrayList<Tarea>()

    lateinit var sinTareas: ImageView
    lateinit var mensaje: TextView
    lateinit var mensje: TextView
    lateinit var tareaView: RecyclerView


    abstract fun actualizar(refreshData: Boolean)

    fun cargarDatos(firebase: Boolean, mDatabaseHelper: DatabaseHelper, db: FirebaseFirestore, tareasListParam: ArrayList<TareaAdapter>, categoria: String, refresh: Boolean, sinTareas: ImageView, tareaView: RecyclerView, mensaje: TextView, mensje: TextView, correoUsuario: String) {
        this.sinTareas = sinTareas
        this.mensaje = mensaje
        this.mensje = mensje
        this.tareaView = tareaView
        val sharedPref = activity!!.getPreferences(Context.MODE_PRIVATE)
        if (firebase) {
            cargarDatosFirebase(db, tareasListParam, categoria, refresh,correoUsuario)
            sharedPref.edit().putBoolean(getString(R.string.user_notified_no_network), false).commit()
        } else {
            cargarDatosSQLite(mDatabaseHelper, tareasListParam, categoria, refresh)
            val userNotified = sharedPref.getBoolean(getString(R.string.user_notified_no_network), false)
            if (!userNotified) {
                val builder = AlertDialog.Builder(context)
                builder.setTitle("Error de conexión")
                builder.setMessage("En este momento no estás conectado a internet, por lo que cargaremos la información local")
                builder.setPositiveButton("Aceptar") { _, _ ->
                }
                val dialog = builder.create()
                dialog.show()
                sharedPref.edit().putBoolean(getString(R.string.user_notified_no_network), true).commit()
            }
        }
    }

    fun cargarDatosFirebase(db: FirebaseFirestore, tareasListParam: ArrayList<TareaAdapter>, categoria: String, refresh: Boolean, correoUsuario:String) {
        val mDatabaseHelper = DatabaseHelper(activity!!)
        this.tareasList = tareasListParam
        db.collection("tutores").document(correoUsuario)
                .collection("tareas$categoria")
                .get()
                .addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        for (document in task.result!!) {
                            val tareaAdapter = document.toObject(TareaAdapter::class.java)
                            tareasList.add(tareaAdapter)

                        }
                        val sharedPref = activity!!.getPreferences(Context.MODE_PRIVATE)
                        sharedPref.edit().putBoolean(getString(R.string.user_notified_no_network), true)
                        actualizar(false)
                        updateView()
                        if (tareasList.isNotEmpty()) {
                            tareasList.sort()
                        }
                    }
                }
                .addOnFailureListener { e ->
                }

        var correoCompartido = ""
        db.collection("tutores").document(correoUsuario).get()
                .addOnCompleteListener { result ->
                    if (result.isSuccessful) {
                        correoCompartido = result.result!!.get("compartido").toString()
                    }

                    db.collection("tutores").document(correoCompartido)
                            .collection("tareas$categoria")
                            .get()
                            .addOnCompleteListener { task ->
                                if (task.isSuccessful) {
                                    for (document in task.result!!) {
                                        val tareaAdapter = document.toObject(TareaAdapter::class.java)
                                        tareasList.add(tareaAdapter)
                                        val tarea = Tarea(tareaAdapter.id, tareaAdapter.categoria, tareaAdapter.titulo, document.get("descripcion").toString(), "${document.get("fecha")}", "${document.get("hora")}", 1)

                                        mDatabaseHelper.addData(tareaAdapter.id, tareaAdapter, tareaAdapter.categoria)
                                    }
                                    val sharedPref = activity!!.getPreferences(Context.MODE_PRIVATE)
                                    sharedPref.edit().putBoolean(getString(R.string.user_notified_no_network), true)
                                    actualizar(false)
                                    updateView()
                                    if (tareasList.isNotEmpty()) {
                                        tareasList.sort()
                                    }
                                }
                            }
                            .addOnFailureListener { e ->
                            }
                }
    }

    private fun cargarDatosSQLite(mDatabaseHelper: DatabaseHelper, tareasListParam: ArrayList<TareaAdapter>, categoria: String, refresh: Boolean) {
        this.tareasList = tareasListParam

        val data = mDatabaseHelper.getTareasXCategoria(categoria)
        while (data.moveToNext()) {
            //get the value from the database in column 1
            //then add it to the ArrayList
            tareasList.add(TareaAdapter(data.getString(0), data.getString(1), "${data.getString(4)} ${data.getString(5)}", data.getString(3), categoria, data.getString(6).toInt(), data.getString(2),""))
        }

        actualizar(false)
        updateView()
        if (tareasList.isNotEmpty()) {
            tareasList.sort()
        }
        //create the list adapter and set the adapter
    }

    private fun updateView() {
        if (tareasList.isEmpty()) {
            tareaView.visibility = View.GONE
            sinTareas.visibility = View.VISIBLE
            mensaje.visibility = View.VISIBLE
            mensje.visibility = View.VISIBLE
        } else {
            tareaView.visibility = View.VISIBLE
            sinTareas.visibility = View.GONE
            mensaje.visibility = View.GONE
            mensje.visibility = View.GONE
        }
    }
}