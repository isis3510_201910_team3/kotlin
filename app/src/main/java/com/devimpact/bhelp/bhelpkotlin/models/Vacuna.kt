package com.devimpact.bhelp.bhelpkotlin.models

import com.google.gson.annotations.SerializedName

class Vacuna(
        @SerializedName("descripcion")val descripcion: String,
        @SerializedName("edad") val edad: Int,
        @SerializedName("dosis") val dosis: Int,
        @SerializedName("aplicada") var aplicada: Int) : Comparable<Vacuna> {

    constructor() : this("", 0,0, 0)

    override fun compareTo(other: Vacuna): Int {
        return this.edad.compareTo(other.edad)
    }
}